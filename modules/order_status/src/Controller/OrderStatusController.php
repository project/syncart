<?php

namespace Drupal\syncart_order_status\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\syncart_order_status\Form\ChangeStatus;

/**
 * Main controller.
 */
class OrderStatusController extends ControllerBase {

  /**
   * Orders status change admin page - DEPRECATED.
   */
  public static function orders() {
    $rows = [];
    $options = [
      0 => '--',
    ];
    $order_service = \Drupal::service('syncart_order_status.order');
    $orders = $order_service->getActiveOrders();
    foreach ($order_service->getStatuses() as $term_id => $term) {
      $options[$term_id] = $term->name->value;
    }
    foreach ($orders as $order_id => $order) {
      $billing_data = $order_service->getBillingInformation($order);
      $items_markup = "";
      foreach ($order->getItems() as $item_id => $item) {
        $quantity = round($item->quantity->value, 0);
        $items_markup .= "<strong>{$item->title->value}</strong><br>";
        $price = round($item->total_price->number, 2);
        $items_markup .= "Кол-во: <strong>{$quantity}</strong><br>";
        $items_markup .= "Общая стоимость: <strong>{$price}{$item->total_price->currency_code}</strong><br>";
      }
      $form = new ChangeStatus($order_id);
      $rows[] = [
        '#' => [
          'data' => [
            '#markup' => "<a href='/admin/commerce/orders/{$order_id}'>{$order->order_number->value}</a>",
          ],
        ],
        'Клиент' => [
          'data' => [
            '#markup' => "<a href='/user/{$order->uid->target_id}'>{$billing_data['name']} {$billing_data['surname']}</a><br>{$billing_data['phone']}<br>{$billing_data['email']}",
          ],
        ],
        'Тип' => $order->type->target_id,
        'Элементы заказа' => [
          'data' => [
            '#markup' => $items_markup,
          ],
        ],
        'Сумма' => round($order->total_price->number, 2) . " " . $order->total_price->currency_code,
        'Завершен' => date('H:i d.m.Y', $order->completed->value),
        'Статус' => [
          'data' => \Drupal::formBuilder()->getForm(
            $form,
            [
              'default' => $order->field_status->target_id,
              'order_id' => $order_id,
              'options' => $options,
            ]
          ),
        ],
      ];
    }
    return [
      'title' => ['#markup' => "<h2>Управление статусами</h2>"],
      'table' => [
        '#type' => 'table',
        '#header' => ['№', 'Клиент', 'Тип', 'Элементы заказа', 'Сумма', 'Завершен', 'Статус'],
        '#rows' => $rows,
        '#attributes' => ['class' => ['table', 'table-striped', 'table-hover']],
        '#allowed_tags' => ['p', 'h2', 'small', 'br'],
      ],
    ];
  }

  /**
   * Orders list page.
   */
  public static function list() {
    $tables = [];
    $order_service = \Drupal::service('syncart_order_status.order');
    foreach ($order_service->getStatuses(TRUE) as $term_id => $term) {
      $tables[$term_id] = [
        'name' => $term->name->value,
        'count' => 0,
        'orders' => '',
      ];
      foreach ($order_service->getOrdersByStatusId($term_id) as $order_id => $order) {
        $tables[$term_id]['count']++;
        $tables[$term_id]['orders'] .= "<div class='fs-4'>{$order->order_number->value}</div>";
      }
    }
    $result = [];
    $result["top"] = ['#markup' => "<div class='container'><div class='row'>"];
    foreach ($tables as $term_id => $data) {
      $result["$term_id-wrapper-top"] = ['#markup' => "<div class='col-4 border-start border-end order-list-wrapper'>"];
      $result["$term_id-title"] = ['#markup' => "<div class='h3 mb-4 fw-bold order-list-title'>{$data['name']} ({$data['count']})</div>"];
      $result["$term_id-orders"] = ['#markup' => $data['orders']];
      $result["$term_id-wrapper-bottom"] = ['#markup' => "</div>"];
    }
    $result["bottom"] = ['#markup' => "</div></div>"];
    return $result;
  }

}
