<?php

namespace Drupal\syncart_order_status\Service;

use Drupal\Core\Entity\EntityTypeManager;

/**
 * Orders service.
 */
class Order {

  /**
   * Drupal\Core\Entity\EntityTypeManager definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * Service construct.
   */
  public function __construct(EntityTypeManager $entity_type_manager) {
    $this->orderStorage = $entity_type_manager->getStorage('commerce_order');
    $this->taxonomyTermStorage = $entity_type_manager->getStorage('taxonomy_term');
  }

  /**
   * Set a specific status to order.
   */
  public function setOrderStatus($order_id, $status_id) : string | NULL {
    $order = $this->orderStorage->load($order_id);
    $status = $this->taxonomyTermStorage->load($status_id);
    if ($order) {
      $order->set('field_status', $status_id);
      $order->save();
      return t('Order status changed to') . " <b>{$status->name->value}</b>";
    }
    return NULL;
  }

  /**
   * Get orders with specific status name.
   */
  public function getOrdersByStatus(string $status_name) : array {
    $entities = [];
    $status_id = $this->getStatusIdByName($status_name);
    if ($status_id !== NULL) {
      $ids = $this->orderStorage->getQuery()->accessCheck(TRUE)
        ->condition('state', 'completed')
        ->condition('field_status', $status_id)
        ->sort('order_number')
        ->execute();
      foreach ($ids as $id) {
        $entities[$id] = $this->orderStorage->load($id);
      }
    }
    return $entities;
  }

  /**
   * Get orders with specific status id.
   */
  public function getOrdersByStatusId(string $status_id) : array {
    $entities = [];
    $ids = $this->orderStorage->getQuery()->accessCheck(TRUE)
      ->condition('state', 'completed')
      ->condition('field_status', $status_id)
      ->sort('order_number')
      ->execute();
    foreach ($ids as $id) {
      $entities[$id] = $this->orderStorage->load($id);
    }
    return $entities;
  }

  /**
   * Get all completed orders.
   */
  public function getActiveOrders() : array {
    $entities = [];
    $ids = $this->orderStorage->getQuery()->accessCheck(TRUE)
      ->condition('state', 'completed')
      ->sort('completed', 'DESC')
      ->execute();
    foreach ($ids as $id) {
      $entities[$id] = $this->orderStorage->load($id);
    }
    return $entities;
  }

  /**
   * Get user data from order.
   */
  public function getBillingInformation(object $order) : array {
    $billing_profile = $order->billing_profile->entity;
    return [
      'name' => $billing_profile->field_customer_name->value ?? '',
      'surname' => $billing_profile->field_customer_surname->value ?? '',
      'phone' => $billing_profile->field_customer_phone->value ?? '',
      'email' => $billing_profile->field_customer_email->value ?? '',
    ];
  }

  /**
   * Get all statuses.
   */
  public function getStatuses(bool $without_hidden = FALSE) : array {
    $entities = [];
    $query = $this->taxonomyTermStorage->getQuery()->accessCheck(TRUE)
      ->condition('status', 1)
      ->condition('vid', 'order_status');
    if ($without_hidden) {
      $or_group = $query->orConditionGroup()
        ->notExists('field_hidden')
        ->condition('field_hidden', 0);
      $query->condition($or_group);
    }
    $ids = $query->execute();
    foreach ($ids as $id) {
      $entities[$id] = $this->taxonomyTermStorage->load($id);
    }
    return $entities;
  }

  /**
   * Get status term id by it's name.
   */
  public function getStatusIdByName(string $status_name) : string | NULL {
    $statuses = $this->getStatuses();
    foreach ($statuses as $term_id => $term) {
      if ($term->name->value == $status_name) {
        return $term_id;
      }
    }
    return NULL;
  }
}
