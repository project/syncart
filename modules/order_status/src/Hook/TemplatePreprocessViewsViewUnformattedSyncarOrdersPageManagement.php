<?php

namespace Drupal\syncart_order_status\Hook;

/**
 * @file
 * Contains \Drupal\syncart_order_status\Hook\TemplatePreprocessViewsViewUnformattedSyncarOrdersPageManagement.
 */

/**
 * Theme.
 */
class TemplatePreprocessViewsViewUnformattedSyncarOrdersPageManagement {

  /**
   * Hook.
   */
  public static function hook(&$variables) {
    if (empty($variables['rows'])) {
      $variables['rows'] = $variables['elements']['#rows'];
    }
    if (empty($variables['view'])) {
      $variables['view'] = $variables['elements']['#view'];
    }
    template_preprocess_views_view_unformatted($variables);
  }

}
