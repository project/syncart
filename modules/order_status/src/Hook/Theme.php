<?php

namespace Drupal\syncart_order_status\Hook;

/**
 * Hooks.
 */
class Theme {

  /**
   * Implements hook_theme().
   */
  public static function hook() {
    return [
      'commerce_order__table_management' => [
        'template' => 'commerce-order--table-management',
        'base hook' => 'commerce_order__table_management',
      ],
      'views_view_unformatted__syncart_orders__page_management' => [
        'render element' => 'elements',
        'template' => 'views-view-unformatted--syncart-orders--page-management',
      ],
    ];
  }

}
