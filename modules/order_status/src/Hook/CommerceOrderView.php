<?php

namespace Drupal\syncart_order_status\Hook;

use Drupal\syncart_order_status\Form\ChangeStatus;

/**
 * @file
 * Contains \Drupal\syncart_order_status\Hook\CommerceOrderView.
 */

/**
 * Theme.
 */
class CommerceOrderView {

  /**
   * Hook.
   */
  public static function hook(&$build, $order, $view_mode) {
    if ($view_mode != 'table_management') {
      return;
    }
    $order_service = \Drupal::service('syncart_order_status.order');
    $order_id = $order->id();
    $fields = self::getFieldsDefinitions();
    $options = self::getFieldOptions('field_status', $fields);
    $form = new ChangeStatus($order_id);
    $build['form_status'] = \Drupal::formBuilder()->getForm(
      $form,
      [
        'default' => $order->field_status->target_id,
        'order_id' => $order_id,
        'options' => $options,
      ]
    );
    $billing_data = $order_service->getBillingInformation($order);
    $items_markup = "";
    foreach ($order->getItems() as $item_id => $item) {
      $quantity = round($item->quantity->value, 0);
      $items_markup .= "<strong>{$item->title->value}</strong><br>";
      $price = round($item->total_price->number, 2);
      $items_markup .= "Кол-во: <strong>{$quantity}</strong><br>";
      $items_markup .= "Общая стоимость: <strong>{$price}{$item->total_price->currency_code}</strong><br>";
    }
    $build['no'] = [
      'data' => [
        '#markup' => "<a href='/admin/commerce/orders/{$order_id}'>{$order->order_number->value}</a>",
      ],
    ];
    $build['client'] = [
      'data' => [
        '#markup' => "<a href='/user/{$order->uid->target_id}'>{$billing_data['name']} {$billing_data['surname']}</a><br>{$billing_data['phone']}<br>{$billing_data['email']}",
      ],
    ];
    $build['items'] = [
      'data' => [
        '#markup' => $items_markup,
      ],
    ];
    $build['sum'] = [
      'data' => [
        '#markup' => round($order->total_price->number, 2) . " " . $order->total_price->currency_code,
      ],
    ];
    $build['date_completed'] = [
      'data' => [
        '#markup' => date('H:i d.m.Y', $order->completed->value),
      ],
    ];
    $build['order_type'] = [
      'data' => [
        '#markup' => $order->type->target_id,
      ],
    ];
  }

  /**
   * Get Fields Definitions.
   */
  private static function getFieldsDefinitions() : array {
    $entity_type = 'commerce_order';
    $bundle = 'default';
    $entityFieldManager = \Drupal::service('entity_field.manager');
    $fields = $entityFieldManager->getFieldDefinitions($entity_type, $bundle);
    return $fields ?? [];
  }

  /**
   * Get Field Options.
   */
  private static function getFieldOptions(string $field_name, array $fields) : array {
    $options = [];
    if (isset($fields[$field_name])) {
      $field = $fields[$field_name];
      if ($field->get('field_type') == 'entity_reference') {
        $options = self::getFieldEntityReferenceOptions($field);
      }
      // @todo Add List field_type.
    }
    return $options;
  }

  /**
   * Get Field Entity Reference Options.
   */
  private static function getFieldEntityReferenceOptions(object $field) : array {
    $options = [];
    $settings = $field->get('settings');
    if (empty($settings)
    || !isset($settings['handler'])
    || !isset($settings['handler_settings'])) {
      return $options;
    }
    $handler = explode(':', $settings['handler']);
    $handler_type = $handler[0] ?? '';
    $handler_entity_type = $handler[1] ?? '';
    if (!$handler_type || !$handler_entity_type) {
      return $options;
    }
    $target_bundles = $settings['handler_settings']['target_bundles'];
    if (empty($target_bundles)) {
      return $options;
    }
    $target_bundle = array_shift($target_bundles);
    switch ($handler_entity_type) {
      case 'taxonomy_term':
        $options = self::getTaxonomyTerms($target_bundle);
        break;

      // @todo Add other entity types.
    }
    return $options;
  }

  /**
   * Get Taxonomy Terms.
   */
  private static function getTaxonomyTerms(string $vid) : array {
    $terms = [0 => '--'];
    $terms_stds = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);
    foreach ($terms_stds as $std) {
      if ($std->status != 1) {
        continue;
      }
      $terms[$std->tid] = $std->name;
    }
    return $terms;
  }

}
