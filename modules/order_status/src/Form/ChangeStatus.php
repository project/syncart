<?php

namespace Drupal\syncart_order_status\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\synhelper\Controller\AjaxResult;

/**
 * Implements the form controller.
 */
class ChangeStatus extends FormBase {
  /**
   * Construct.
   */
  public function __construct($order_id) {
    $this->order_id = $order_id;
    $this->wrapper = "change-order-$order_id-status-form";
  }

  /**
   * AJAX ajaxPrev.
   */
  public function ajaxSubmit(array &$form, $form_state) {
    $extra = $form_state->extra;
    $status = $form_state->getValue('status');
    $res = \Drupal::service('syncart_order_status.order')->setOrderStatus($extra['order_id'], $status);
    return AjaxResult::ajax($this->wrapper, $res);
  }

  /**
   * Build the simple form.
   */
  public function buildForm(array $form, FormStateInterface $form_state, $extra = NULL) {
    $form_state->extra = $extra;
    $form_state->setCached(FALSE);
    $form["status"] = [
      '#default_value' => $extra['default'],
      '#type' => 'select',
      '#attributes' => ['class' => ['inline']],
      '#options' => $extra['options'],
      '#ajax'   => [
        'callback' => '::ajaxSubmit',
        'effect'   => 'fade',
        'progress' => ['type' => 'throbber', 'message' => NULL],
      ],
      '#suffix' => "<div id='{$this->wrapper}'></div>",
    ];
    return $form;
  }

  /**
   * Implements a form submit handler.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRebuild(TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return "change_order_status_{$this->order_id}";
  }

}
