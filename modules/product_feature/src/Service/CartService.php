<?php

namespace Drupal\syncart_product_feature\Service;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Component\Serialization\Json;
use Drupal\commerce_order\Entity\OrderItemInterface;
use Drupal\commerce_order\Adjustment;
use Drupal\syncart\Service\ProductService;

/**
 * Class Cart Service.
 */
class CartService {

  const ADJUSTMENT_TYPE = 'syncart_product_feature';

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The product service.
   *
   * @var \Drupal\syncart\Service\ProductService
   */
  protected $productService;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\syncart\Service\ProductService $product_service
   *   The product service.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    ProductService $product_service
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->productService = $product_service;
  }

  /**
   * Recreate adjustments.
   */
  public function recreateAdjustments(OrderItemInterface $order_item) {
    foreach ($order_item->getAdjustments() as $adjustment) {
      $adjustment_type = $adjustment->getType();
      if ($adjustment_type != self::ADJUSTMENT_TYPE) {
        continue;
      }
      $order_item->removeAdjustment($adjustment);
    }
    $selected_products = Json::decode($order_item->field_json->value ?? '[]');
    foreach ($selected_products as $selected_product) {
      /** @var \Drupal\commerce_product\Entity\ProductInterface $commerce_product */
      $commerce_product = $this->entityTypeManager
        ->getStorage('commerce_product')
        ->load($selected_product['id']);
      $variation = $this->productService->getFirstAvailableVariation($commerce_product);
      $quantity = $order_item->getQuantity();
      $order_item->addAdjustment(new Adjustment([
        'type' => self::ADJUSTMENT_TYPE,
        'label' => $selected_product['title'],
        'amount' => $variation->getPrice()->multiply($quantity),
      ]));
    }
  }

}
