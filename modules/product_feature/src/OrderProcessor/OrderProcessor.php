<?php

namespace Drupal\syncart_product_feature\OrderProcessor;

use Drupal\commerce_order\Adjustment;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\Entity\OrderItemInterface;
use Drupal\commerce_order\OrderProcessorInterface;
use Drupal\commerce_price\Price;

/**
 *  Order processor.
 */
class OrderProcessor implements OrderProcessorInterface {

  const ORDER_ITEM_BUNDLE_PRODUCT_FEATURE = 'product_feature';

  /**
   * {@inheritdoc}
   */
  public function process(OrderInterface $order) {
    $this->cart = $order;
    $this->service = \Drupal::service('syncart_product_feature.cart');
    $this->recreateAdjustments();
  }
  
  /**
   * {@inheritdoc}
   */
  public function recreateAdjustments() {
    $order_items = $this->cart->getItems();
    foreach ($order_items as $order_item) {
      if ($this->checkOrderItemBundle($order_item)) {
        $this->service->recreateAdjustments($order_item);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function checkOrderItemBundle(OrderItemInterface $order_item) : bool {
    if ($order_item->bundle() == self::ORDER_ITEM_BUNDLE_PRODUCT_FEATURE) {
      return TRUE;
    }
    return FALSE;
  }

}