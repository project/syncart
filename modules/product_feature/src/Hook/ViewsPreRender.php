<?php

namespace Drupal\syncart_product_feature\Hook;

use Drupal\Component\Serialization\Json;
use Drupal\views\ViewExecutable;
use Drupal\commerce_order\Entity\OrderItemInterface;

/**
 * Implements hook_views_pre_render.
 */
class ViewsPreRender {

  /**
   * Implements hook.
   */
  public static function hook(ViewExecutable $view) {
    if ($view->id() != 'commerce_order_item_table') {
      return;
    }
    foreach ($view->result as $value) {
      if (!property_exists($value, '_entity')) {
        continue;
      }
      /** @var \Drupal\commerce_order\Entity\OrderItemInterface $order_item */
      $order_item = $value->_entity;
      $features = self::getOrderItemFeatures($order_item);
      if (empty($features)) {
        continue;
      }
      $currency_formatter = \Drupal::service('commerce_price.currency_formatter');
      $currency_code = $order_item->getPurchasedEntity()
        ->getPrice()
        ->getCurrencyCode();
      $features_lines = array_map(
        fn($feature) => $feature['title'] . ' [' . $currency_formatter->format($feature['price_number'], $currency_code, []) . ']', $features
      );
      $title = implode("\n", [
        $order_item->get('title')->value,
        'Комплектация товара: ',
        implode("\n", $features_lines),
      ]);
      $order_item->set('title', $title);
    }
  }

  /**
   * Get order_item Features.
   */
  private static function getOrderItemFeatures(OrderItemInterface $order_item) : array {
    if (!$order_item->hasField('field_json')) {
      return [];
    }
    elseif ($order_item->field_json->isEmpty()) {
      return [];
    }
    return Json::decode($order_item->field_json->value);
  }

}
