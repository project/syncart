<?php

namespace Drupal\syncart_pos\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Pos main controller.
 */
class PosController extends ControllerBase {

  /**
   * Установка coockie для POS терминала.
   */
  public static function setCookie() {
    $cache_context = new Cookie('cache_context', 'pos', 32516217114, '/');
    $response = new RedirectResponse('/');
    /** @var $response \Symfony\Component\HttpFoundation\RedirectResponse */
    $response->headers->setCookie($cache_context);
    return $response;
  }
}
