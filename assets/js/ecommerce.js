/**
 * @file
 * ecommerce.js.
 */

window.addEventListener("DOMContentLoaded", function () {
  if (drupalSettings.metrika.dataLayerPurchase) {
    dataLayerEvent("purchase", drupalSettings.metrika.dataLayerPurchase);
  }
});

function dataLayerEvent(event, element = {}, quantity = 1) {
  switch (event) {
    case "view":
      dataLayerView(element, quantity);
      break;
    case "add":
      dataLayerAdd(element, quantity);
      break;
    case "remove":
      dataLayerRemove(element, quantity);
      break;
    case "purchase":
      dataLayerPurchase(element);
      break;
  }
}

function dataLayerView(element, quantity) {
  if (drupalSettings.metrika.ga4 || drupalSettings.metrika.ya_counter) {
    let event = "view_item";
    gtag("event", event, {
      currency: element.currency,
      items: [
        {
          item_id: element.vid,
          item_name: element.title,
          price: element.number,
          item_category: element.catalog,
          quantity: quantity,
        },
      ],
      send_to: drupalSettings.metrika.ga4,
    });
  }
}

function dataLayerAdd(element, quantity) {
  if (drupalSettings.metrika.ga4 || drupalSettings.metrika.ya_counter) {
    let event = "add_to_cart";
    gtag("event", event, {
      currency: element.currency,
      items: [
        {
          item_id: element.vid,
          item_name: element.title,
          price: element.number,
          item_category: element.catalog,
          quantity: quantity,
        },
      ],
      send_to: drupalSettings.metrika.ga4,
    });
  }
}

function dataLayerRemove(element, quantity) {
  if (drupalSettings.metrika.ga4 || drupalSettings.metrika.ya_counter) {
    let event = "remove_from_cart";
    gtag("event", event, {
      currency: element.currency,
      items: [
        {
          item_id: element.vid,
          item_name: element.title,
          price: element.number,
          item_category: element.catalog,
          quantity: quantity,
        },
      ],
      send_to: drupalSettings.metrika.ga4,
    });
  }
}

function dataLayerPurchase(element) {
  if (drupalSettings.metrika.ga4 || drupalSettings.metrika.ya_counter) {
    let event = "purchase";
    gtag("event", event, element);
  }
}
