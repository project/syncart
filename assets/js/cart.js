/**
 * @file
 * Vue app for cart.
 */
(function ($) {
  "use strict";

  if (document.readyState != "loading") {
    vueCartApp("#app-cart");
  } else {
    window.addEventListener("DOMContentLoaded", function () {
      vueCartApp("#app-cart");
    });
  }

  function vueCartApp(sel) {
    var app;
    const elements = document.querySelectorAll(sel);
    const each = Array.prototype.forEach;
    each.call(elements, function (el, i) {
      let comp_template = el.querySelector("cart");
      app = Vue.createApp({ delimiters, comments, el })
        .use(store)
        .use($cookies);
      app.component("favorite-field", {
        template: `<div
        class="cart-button cart-button--fav"
        :class="fav ? 'cart-button--fav-in' : ''"
        style="display:none"
        v-show="products_loaded"
        @click="toggleFav"
        >
          <i class="far fa-heart"></i>
        </div>`,
        delimiters: delimiters,
        props: {
          pid: 0,
        },
        data() {
          return {
            products_loaded: false,
            fav: false,
          };
        },
        methods: {
          toggleFav() {
            if (this.fav) {
              this.deleteFav();
            } else {
              this.addFav();
            }
          },
          addFav() {
            this.fav = true;
            this.like_clicked = true;
            var favorites = this.$cookies.get("favorites");
            if (typeof favorites !== "object" || favorites == null) {
              favorites = {};
            }
            favorites[this.pid] = true;
            this.$cookies.set("favorites", favorites, "30d");
            var total = Object.keys(favorites).length;
            this.$store.commit("updateFavorite", { total });
          },
          deleteFav() {
            this.fav = false;
            this.like_clicked = false;
            var favorites = this.$cookies.get("favorites");
            if (typeof favorites !== "object" || favorites == null) {
              favorites = {};
            }
            delete favorites[this.pid];
            this.$cookies.set("favorites", favorites, "30d");
            if (window.location.pathname == "/favorites") {
              window.location.reload();
            } else {
              var total = Object.keys(favorites).length;
              this.$store.commit("updateFavorite", { total });
            }
          },
        },
        created() {
          this.products_loaded = true;
          let favorites = this.$cookies.get("favorites");
          if (
            favorites !== null &&
            typeof favorites == "object" &&
            favorites.hasOwnProperty(this.pid)
          ) {
            this.fav = true;
          }
          if (localStorage.watched) {
            var array = localStorage.watched.split("|");
          } else {
            var array = [];
          }
          let index = array.indexOf(this.pid.toString());
          if (index === -1) {
            array.push(this.pid);
            localStorage.watched = array.join("|");
          }
        },
      });
      app.component("cart", {
        template: comp_template,
        delimiters: delimiters,
        data() {
          return {
            quantities: {},
            ready: false,
            init: false,
            computed: false,
          };
        },
        computed: {
          cart() {
            return this.$store.state.cart;
          },
        },
        watch: {
          cart(new_cart, old_cart) {
            if (!this.init) {
              this.init = true;
              return;
            }
            if (!this.computed) {
              this.computed = true;
              return;
            }
            this.fillQuantities(new_cart);
            this.$store.dispatch("sendDataLayers", {
              new_cart: new_cart,
              old_cart: old_cart,
            });
          },
        },
        methods: {
          initCart() {
            let load_path = "/api/cart-load";
            let path = drupalSettings.path.currentPath;
            let arg = path.split("/");
            if (arg.length == 3) {
              if (arg[0] == "checkout" && parseInt(arg[1]) > 0) {
                load_path += "/" + arg[1];
              }
            }
            axios.get(load_path).then((response) => {
              this.$store.commit("updateCart", response.data);
              this.fillQuantities(this.$store.state.cart);
              if (response.data.quantity > 0) {
                this.ready = true;
              }
            });
          },
          fillQuantities(cart) {
            this.quantities = {};
            cart.items.forEach((item) => {
              this.quantities[item.id] = item.quantity;
            });
          },
          changeItemQuantity(sign, item) {
            let new_quantity = +item.quantity;
            switch (sign) {
              case "plus":
                new_quantity += item.qstep;
                break;
              case "minus":
                new_quantity -= item.qstep;
                break;
            }
            if (new_quantity > 0) {
              let vid = item.vid;
              this.$store.commit("propertiesSet", { vid });
              this.$store.dispatch("setItemQuantity", {
                item_id: item.id,
                quantity: new_quantity,
              });
            } else {
              this.removeItem(item.id);
            }
          },
          setItemQuantity(item) {
            if (this.quantities[item.id] > 0) {
              this.$store.dispatch("setItemQuantity", {
                item_id: item.id,
                quantity: this.quantities[item.id],
              });
            } else {
              this.removeItem(item.id);
            }
          },
          setItemNote(item) {
            this.$store.dispatch("setItemNote", { item: item });
          },
          removeItem(item_id) {
            this.$store
              .dispatch("removeItem", { item_id: item_id })
              .then(() => {
                this.ready = false;
                this.initCart();
              });
          },
          checkQuantities() {
            if (this.cart.items[0].checkstore) {
              this.$store.dispatch("refreshStock").then(() => {
                let quantity_available = true;
                this.cart.items.forEach((item) => {
                  if (item.quantity > item.stock) {
                    quantity_available = false;
                  }
                });
                if (quantity_available) {
                  window.location.assign(
                    "/checkout/" + this.cart.id + "/order_information"
                  );
                }
              });
            } else {
              window.location.assign(
                "/checkout/" + this.cart.id + "/order_information"
              );
            }
          },
          getProductName(item) {
            let title = item.title;
            if (item.uname != "") {
              title += ", " + item.uname;
            }
            return title;
          },
          adjustmentEmpty(item) {
            if (item.adjustments == undefined) {
              return true;
            } else if (item.adjustments.length == 0) {
              return true;
            } else {
              let adjustments = item.adjustments.filter(
                (adjustment) => adjustment.type != "syncart_product_feature"
              );
              if (adjustments.length == 0) {
                return true;
              }
            }
            return false;
          },
          getItemPrice(item) {
            let feature_amount = this.getFeatureAmount(item);
            let price = +item.price_number;
            price += +feature_amount;
            return price;
          },
          getFormattedItemPrice(item) {
            let price = this.getItemPrice(item);
            return this.intlFormatPrice(price, item.currency);
          },
          getFeatureAmount(item) {
            if (this.isEmptyObject(item.selected_feature_products)) {
              return 0;
            }
            let amount = 0;
            for (const property in item.selected_feature_products) {
              const product = item.selected_feature_products[property];
              amount += +product.price_number;
            }
            return amount;
          },
          getCorrectionAmount(item) {
            if (this.adjustmentEmpty(item)) {
              return 0;
            }
            let amount = 0;
            let adjustments = item.adjustments.filter(
              (adjustment) => adjustment.type != "syncart_product_feature"
            );
            adjustments.forEach((adjustment) => {
              amount += +adjustment.number;
            });
            return amount;
          },
          getFormattedCorrectionAmount(item) {
            let price = this.getCorrectionAmount(item);
            return this.intlFormatPrice(price, item.currency);
          },
          getItemAmount(item) {
            let сorrection_amount = this.getCorrectionAmount(item);
            let price = this.getItemPrice(item);
            let amount = price * item.quantity;
            amount += +сorrection_amount;
            return amount;
          },
          getFormattedItemAmount(item) {
            let price = this.getItemAmount(item);
            return this.intlFormatPrice(price, item.currency);
          },
          getItemTotalPrice(item) {
            let price = this.getItemPrice(item);
            let total = price * item.quantity;
            return total;
          },
          getFormattedItemTotalPrice(item) {
            let price = this.getItemTotalPrice(item);
            return this.intlFormatPrice(price, item.currency);
          },
          numbersOnly(event) {
            event = event ? event : window.event;
            var char_code = event.which ? event.which : event.keyCode;
            if (
              char_code > 31 &&
              (char_code < 48 || char_code > 57) &&
              char_code !== 46
            ) {
              event.preventDefault();
            } else {
              return true;
            }
          },
          numberFormat(
            number,
            step,
            decimals = 2,
            dec_point = ".",
            thousands_sep = " "
          ) {
            let s_number =
              Math.abs(parseInt((number = (+number || 0).toFixed(decimals)))) +
              "";
            let len = s_number.length;
            let tchunk = len > 3 ? len % 3 : 0;
            let ch_first = tchunk
              ? s_number.substr(0, tchunk) + thousands_sep
              : "";
            let ch_rest = s_number
              .substr(tchunk)
              .replace(/(\d\d\d)(?=\d)/g, "$1" + thousands_sep);
            let ch_last = decimals
              ? dec_point +
                (Math.abs(number) - s_number).toFixed(decimals).slice(2)
              : "";
            if (step == 1) {
              ch_last = "";
            }
            return ch_first + ch_rest + ch_last;
          },
          isEmpty(value) {
            if (!!value && value instanceof Array) {
              return value.length < 1;
            }
            if (!!value && typeof value === "object") {
              for (var key in value) {
                if (hasOwnProperty.call(value, key)) {
                  return false;
                }
              }
            }
            return !value;
          },
          isEmptyObject(current_object) {
            return (
              current_object &&
              Object.keys(current_object).length === 0 &&
              current_object.constructor === Object
            );
          },
          intlFormatPrice(price, currency) {
            // Create our number formatter.
            const formatter = new Intl.NumberFormat("ru-RU", {
              style: "currency",
              currency: currency,
              // These options are needed to round to whole numbers if that's what you want.
              minimumFractionDigits: 0, // (this suffices for whole numbers, but will print 2500.10 as $2,500.1)
              //maximumFractionDigits: 0, // (causes 2500.99 to be printed as $2,501)
            });
            return formatter.format(price);
          },
        },
        created() {
          this.initCart();
        },
      });
      app.mount(el);
    });
  }

  function phoneInputMask() {
    const lang = drupalSettings.path.currentLanguage;
    if (lang == "ru") {
      $(".field--name-field-customer-phone input").inputmask(
        "+7 (999) 999-99-99",
        {
          clearIncomplete: true,
          showMaskOnHover: false,
        }
      );
    } else {
      $(".field--name-field-customer-phone input").inputmask(
        "+9 (999) 999-99-99",
        {
          clearIncomplete: true,
          showMaskOnHover: false,
        }
      );
    }
  }

  if (typeof Drupal === "object") {
    Drupal.behaviors.syncart = {
      attach: function (context) {
        let action = $(context).attr("action");
        phoneInputMask();
        if (action && action.length > 1) {
          if (action.substring(0, 10) == "/checkout/") {
            vueCartApp("#app-cart"); // reinit cart
          }
        }
      },
    };
  }

  String.prototype.replaceAll = function (search, replacement) {
    return this.split(search).join(replacement);
  };
})(jQuery);
