// /**
//  * @file
//  * Vue app for favorites.
//  */

(function () {
  "use strict";
  
    if (document.readyState != 'loading') {
      if (document.getElementById('favorites') !== null) {
        vueCartApp("#favorites");
      }
    } else {
      window.addEventListener("DOMContentLoaded", function () {
        if (document.getElementById('favorites') !== null) {
          vueCartApp("#favorites");
        }
      });
    }
    
    function vueCartApp(sel) {
      var app;
      const elements = document.querySelectorAll(sel);
      const each = Array.prototype.forEach;
      each.call(elements, function (el, i) {
        let comp_template = el.querySelector('favorites');
        app = Vue.createApp({ delimiters, comments, el }).use(store).use($cookies);
        app.component("favorites", {
          template: comp_template,
          delimiters: delimiters,
          props: {
            ids: Array,
          },
          data() {
            return {
              ready: false,
              loading: false,
              inCart: false,
            };
          },
          methods: {
            toCart() {
              this.loading = true;
              axios.post("/cart/add-items", { vids: this.ids }).then(
                (response) => {
                  this.loading = false;
                  this.inCart = true;
                  this.$store.commit("updateCart", response.data);
                },
              );
            },
            clearFavorites() {
              this.$cookies.set("favorites", [], "30d");
              window.location.reload();
            },
          },
          created() {
            this.ready = true;
          },
        });
        app.mount(el);
      });
    }
  
})();
