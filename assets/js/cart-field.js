/**
 * @file
 * Vue app for cart-field.
 */

(function ($) {
  $(document).ready(function () {
    siteReady();
  });

  if (document.readyState != 'loading') {
    vueCartApp('#product-info');
    setTimeout(() => {
      vueCartApp('.product-teaser');
      vueCartApp('.product-preview');
    }, 1000);
  } else {
    window.addEventListener('DOMContentLoaded', function () {
      vueCartApp('#product-info');
      setTimeout(() => {
        vueCartApp('.product-teaser');
        vueCartApp('.product-preview');
      }, 1000);
    });
  }
  function vueCartApp(sel) {
    var app;
    const elements = document.querySelectorAll(sel);
    const each = Array.prototype.forEach;
    each.call(elements, function (el, i) {
      let comp_template = el.querySelector('cart-field');
      let comp_template_fav = el.querySelector('favorite-field');
      app = Vue.createApp({ delimiters, comments, el }).use(store).use($cookies);
      if (comp_template) {
        app.component('cart-field', {
          template: comp_template,
          delimiters: delimiters,
          props: {
            pid: 0,
            uid: 0,
            variations: false,
            attributes: false,
            settings: false,
          },
          data() {
            return {
              ready: false,
              init: false,
              computed: false,
              loading: false,
              id: false,
              vid: false,
              old_price: false,
              price: false,
              price_number: false,
              donation: false,
              note: '',
              attribute_сount: false,
              active_ids: [],
              enable_ids: [],
              quantity: 1,
              stock: 0,
              stores: [],
              selected_features: {},
            };
          },
          computed: {
            order_item_id() {
              let order_item = this.cart.items.find(
                (item) =>
                  item.vid == this.vid && this.json_selected_products(item.selected_feature_products) == this.json_selected_products(this.selected_products)
              );
              if (order_item) {
                return order_item.id;
              }
              null;
            },
            in_cart() {
              if (!Array.isArray(this.cart.items)) {
                return false;
              }
              let index = this.cart.items.findIndex((item) => item.vid == this.vid);
              return index > -1;
            },
            isFullstate() {
              return this.settings.fullstate;
            },
            isTeaser() {
              return !this.settings.fullstate;
            },
            showQuantityWidget() {
              return this.settings.showquantity && this.in_cart;
              // if (this.isTeaser) {
              // } else if (this.isFullstate) {
              //   return this.settings.showquantity;
              // }
              // return false;
            },
            showAddToCartButton() {
              return !this.in_cart;
              // if (this.isTeaser) {
              // } else if (this.isFullstate) {
              //   return this.settings.showquantity || !this.in_cart;
              // }
              // return false;
            },
            showGoToCartButton() {
              return !this.settings.showquantity && this.in_cart;
            },
            showGoToProductButton() {
              return this.isTeaser && this.variations.length != 1;
            },
            total_price() {
              let price = (parseFloat(this.price_number) + parseFloat(this.selected_features_price)).toFixed(6);
              return this.intlFormatPrice(price);
            },
            selected_products() {
              let products = {};
              for (var feature_product_id in this.selected_features) {
                let value = this.selected_features[feature_product_id];
                if (value != 'none' && value) {
                  let ids = feature_product_id.split('-');
                  let feature_id = ids[0];
                  let feature = this.settings.features.find((f) => f.id == feature_id);
                  let product_id = value;
                  if (ids.length == 2) {
                    product_id = ids[1];
                  }
                  let product = feature.products.find((p) => p.id == product_id);
                  products[product_id] = product;
                }
              }
              return products;
            },
            selected_features_price() {
              let price = 0;
              for (var product_id in this.selected_products) {
                let product = this.selected_products[product_id];
                price = (parseFloat(price) + parseFloat(product.price_number)).toFixed(6);
              }
              return price;
            },
            selected_features_overtaken() {
              if (this.isEmptyObject(this.selected_products)) {
                return false;
              }
              for (const key in this.selected_products) {
                if (+this.selected_products[key].stock < +this.quantity) {
                  return true;
                }
              }
              return false;
            },
            overtaken() {
              if (!this.settings.checkstore) {
                return false;
              } else if (this.selected_features_overtaken) {
                return true;
              }
              return this.quantity > this.stock;
            },
            cart() {
              return this.$store.state.cart;
            },
            variationSum() {
              let sum = this.quantity * this.price_number;
              return sum.toFixed(2);
            },
            availability() {
              if (this.variations.length == 0) {
                return 0;
              }
              if (!this.settings.status) {
                return 0;
              }
              if (!this.settings.checkstore) {
                return 1;
              }
              if (this.stock < this.settings.qstep) {
                return 0;
              }
              return 1;
            },
          },
          watch: {
            cart(new_cart, old_cart) {
              if (!this.init) {
                this.init = true;
                return;
              }
              if (!this.computed) {
                this.computed = true;
                return;
              }
              this.$store.dispatch('sendDataLayers', {
                new_cart: new_cart,
                old_cart: old_cart,
              });
            },
            selected_products(selected_products) {
              this.$store.commit('propertiesSet', { selected_products });
            },
            vid(vid) {
              this.$store.commit('propertiesSet', { vid });
            },
          },
          methods: {
            json_selected_products(selected_products) {
              if (this.isEmpty(selected_products)) {
                return '{}';
              }
              return JSON.stringify(selected_products);
            },
            initCart() {
              this.$store.dispatch('initCart').then(() => {
                const item = this.cart.items.find((item) => item.vid == this.vid && item.quantity > 0);
                if (item) {
                  this.quantity = item.quantity;
                  this.selectVariation(item);
                }
                this.ready = true;
              });
            },
            addToCart() {
              if (this.loading || !this.vid || (this.settings.donation && this.donation < 1)) {
                return false;
              }
              this.loading = true;
              let vid = this.vid;
              if (this.isTeaser && this.in_cart) {
                this.quantity += this.settings.qstep;
              }
              let quantity = this.quantity;
              let donation = this.donation;
              let payload = { vid, quantity, donation };
              this.$store.dispatch('addToCart', payload).then((response) => {
                this.loading = false;
                const item = this.cart.items.find((item) => item.vid == vid && item.quantity > 0);
                if (item) {
                  dataLayerEvent('add', item, this.quantity);
                }
              });
            },
            changeVariationQuantity(sign) {
              this.$store.dispatch('refreshStock').then(() => {
                let new_quantity = +this.quantity;
                switch (sign) {
                  case 'plus':
                    new_quantity += this.settings.qstep;
                    break;
                  case 'minus':
                    new_quantity -= this.settings.qstep;
                    break;
                }
                console.log('1');

                if (new_quantity > 0) {
                  // if (this.isTeaser) {
                  this.$store
                    .dispatch('setVariationQuantity', {
                      vid: this.vid,
                      quantity: new_quantity,
                    })
                    .then(() => {
                      let item = this.cart.items.find((item) => item['vid'] == this.vid);
                      if (item) {
                        this.setNewQuantity(new_quantity, item.stock);
                      }
                    });
                  // }
                  // else {
                  //   this.setNewQuantity(new_quantity);
                  // }
                } else {
                  this.removeItem(this.order_item_id);
                }
              });
            },
            setNewQuantity(new_quantity, stock) {
              if (this.settings.checkstore) {
                if (typeof stock == 'undefined') {
                  this.quantity = +new_quantity;
                } else {
                  this.quantity = +new_quantity <= +stock ? new_quantity : stock;
                }
              } else {
                this.quantity = +new_quantity;
              }
            },
            setVariationQuantity() {
              if (this.quantity > 0) {
                this.$store.dispatch('setVariationQuantity', {
                  vid: this.vid,
                  quantity: this.quantity,
                });
              } else {
                this.removeItem(this.order_item_id);
              }
            },
            removeItem(item_id) {
              this.$store.dispatch('removeItem', { item_id }).then(() => {
                this.ready = false;
                this.quantity = 1;
                this.initCart();
              });
            },
            selectVariation(variation) {
              this.vid = variation.vid;
              this.old_price = variation.oldprice_format;
              this.price = variation.price_format;
              this.price_number = variation.price_number;
              this.stock = variation.stock;
              this.donation = parseInt(variation.price);
              this.stores = variation.stores;
              if (this.settings.checkstore) {
                if (this.quantity > variation.stock) {
                  this.quantity = variation.stock;
                }
              }
            },
            selectAttribute(id, name) {
              this.vid = 0;
              this.quantity = 1;
              var xvar = [];
              if (this.settings.masterarray.includes(id)) {
                this.$store.commit('updateColor', id);
                this.enable_ids = [];
                this.active_ids = [];
                this.active_ids.push(id);
              } else {
                if (!this.active_ids.includes(id)) {
                  for (var attr in this.attributes) {
                    let needclear = false;
                    this.attributes[attr].forEach((v) => {
                      if (id == v.id) {
                        needclear = true;
                      }
                    });
                    if (needclear) {
                      this.attributes[attr].forEach((v) => {
                        if (this.active_ids.includes(v.id)) {
                          this.active_ids.splice(this.active_ids.indexOf(v.id), 1);
                        }
                      });
                      this.active_ids.push(id);
                    }
                  }
                }
              }
              let masterId = 0;
              this.active_ids.forEach((v) => {
                if (this.settings.masterarray.includes(v)) {
                  masterId = v;
                }
              });
              this.variations.forEach((v) => {
                let q = 0;
                v.attributes.forEach((a) => {
                  if (masterId == a.id) {
                    q++;
                  }
                });
                if (q > 0) {
                  v.attributes.forEach((a) => {
                    if (this.settings.masterarray.includes(a.id)) {
                      if (this.settings.checkstore) {
                        if (v.stock > 0) {
                          xvar.push(v);
                        }
                      } else {
                        xvar.push(v);
                      }
                    }
                  });
                }
              });
              this.enable_ids = [];
              xvar.forEach((v) => {
                v.attributes.forEach((a) => {
                  if (!this.enable_ids.includes(a.id)) {
                    this.enable_ids.push(a.id);
                  }
                });
              });
              // Добавляем в доступные опции основной атрибут
              this.settings.masterarray.forEach((m) => {
                if (!this.enable_ids.includes(m)) {
                  this.enable_ids.push(m);
                }
              });
              // Убираем "лишнее"
              for (var attr in this.attributes) {
                if (attr != name && !this.settings.masterarray.includes(this.attributes[attr][0].id)) {
                  this.attributes[attr].forEach((v) => {
                    let dis = true;
                    xvar.forEach((x) => {
                      let check = false;
                      x.attributes.forEach((a) => {
                        if (a.id == id) {
                          check = true;
                        }
                      });
                      if (check) {
                        x.attributes.forEach((a) => {
                          if (a.id == v.id) {
                            dis = false;
                          }
                        });
                      }
                    });
                    if (dis) {
                      if (this.enable_ids.includes(v.id)) {
                        this.enable_ids.splice(this.enable_ids.indexOf(v.id), 1);
                      }
                      if (this.active_ids.includes(v.id)) {
                        this.active_ids.splice(this.active_ids.indexOf(v.id), 1);
                      }
                    }
                  });
                }
              }
              if (this.active_ids.length == this.attribute_сount) {
                xvar.forEach((v) => {
                  let q = 0;
                  v.attributes.forEach((a) => {
                    if (this.active_ids.includes(a.id)) {
                      q++;
                    }
                  });
                  if (q == this.active_ids.length) {
                    this.selectVariation(v);
                  }
                });
              }
              for (var attr in this.attributes) {
                if (
                  this.attributes[attr].length == 1 &&
                  !this.active_ids.includes(this.attributes[attr][0].id) &&
                  !this.settings.masterarray.includes(this.attributes[attr][0].id)
                ) {
                  this.selectAttribute(this.attributes[attr][0].id, attr);
                }
              }
            },
            attributeClasses(attr) {
              let classes = ' variations-parameter__item--' + attr.label;
              this.active_ids.includes(attr.id) ? (classes += ' variations-parameter__item--active') : (classes += '');
              !this.enable_ids.includes(attr.id) && !this.enable_ids.length == 0 ? (classes += ' variations-parameter__item--disabled') : (classes += '');
              return classes;
            },
            setFeaturesDefaultValues() {
              for (var feature_id in this.settings.features) {
                let feature = this.settings.features[feature_id];
                this.setFeatureDefaultValue(feature);
              }
            },
            setFeatureDefaultValue(feature) {
              if (feature.widget != 'select') {
                return;
              } else if (feature.none) {
                this.selected_features[feature.id] = 'none';
              } else {
                let first_feature_product = this.getFirst(feature.products);
                this.selected_features[feature.id] = first_feature_product.id;
              }
            },
            numberFormat(number, step, decimals = 2, dec_point = '.', thousands_sep = ' ') {
              let s_number = Math.abs(parseInt((number = (+number || 0).toFixed(decimals)))) + '';
              let len = s_number.length;
              let tchunk = len > 3 ? len % 3 : 0;
              let ch_first = tchunk ? s_number.substr(0, tchunk) + thousands_sep : '';
              let ch_rest = s_number.substr(tchunk).replace(/(\d\d\d)(?=\d)/g, '$1' + thousands_sep);
              let ch_last = decimals ? dec_point + (Math.abs(number) - s_number).toFixed(decimals).slice(2) : '';
              if (step == 1) {
                ch_last = '';
              }
              return ch_first + ch_rest + ch_last;
            },
            numbersOnly(event) {
              event = event ? event : window.event;
              var charCode = event.which ? event.which : event.keyCode;
              if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode !== 46) {
                event.preventDefault();
              } else {
                return true;
              }
            },
            isNull(value) {
              return typeof value === 'object' && !value;
            },
            isEmpty(value) {
              if (!!value && value instanceof Array) {
                return value.length < 1;
              }
              if (!!value && typeof value === 'object') {
                for (var key in value) {
                  if (hasOwnProperty.call(value, key)) {
                    return false;
                  }
                }
              }
              return !value;
            },
            isEmptyObject(current_object) {
              return current_object && Object.keys(current_object).length === 0 && current_object.constructor === Object;
            },
            getFormattedPrice(price) {
              if (price == 0) {
                return '';
              }
              return this.intlFormatPrice(price);
            },
            intlFormatPrice(price) {
              // Create our number formatter.
              const formatter = new Intl.NumberFormat('ru-RU', {
                style: 'currency',
                currency: 'RUB',
                // These options are needed to round to whole numbers if that's what you want.
                minimumFractionDigits: 0, // (this suffices for whole numbers, but will print 2500.10 as $2,500.1)
                //maximumFractionDigits: 0, // (causes 2500.99 to be printed as $2,501)
              });
              return formatter.format(price);
            },
            getFirst(array) {
              return array.find((element) => element !== undefined);
            },
          },
          mounted() {
            this.ready = true;
            if (typeof this.attributes == 'object') {
              this.attribute_сount = 0;
              for (var attr in this.attributes) {
                this.attribute_сount++;
              }
            }
            let key = 0;
            let teaser = el.closest('.catalog-col');
            if (teaser) {
              let vid = teaser.getAttribute('data-vid');
              this.variations.forEach((v, i) => {
                if (v.vid == vid) {
                  key = i;
                }
              });
            }
            if (this.settings.checkstore) {
              if (this.variations[key].stock < 1) {
                this.variations.forEach((element, index) => {
                  if (element.stock > 0) {
                    key = index;
                  }
                });
              }
            }
            if (typeof this.variations[key] == 'undefined') {
              return;
            }
            if (!teaser) {
              dataLayerEvent('view', this.variations[key]);
            }
            this.selectVariation(this.variations[key]);
            if (this.settings.fullstate) {
              this.variations[key].attributes.forEach((v) => {
                if (this.settings.masterarray.includes(v.id)) {
                  this.$store.commit('updateColor', v.id);
                }
                this.selectAttribute(v.id, v.label);
              });
            }
            this.initCart();
            this.setFeaturesDefaultValues();
          },
        });
      }
      if (comp_template_fav) {
        app.component('favorite-field', {
          template: comp_template_fav,
          delimiters: delimiters,
          props: {
            data: Object,
          },
          data() {
            return {
              products_loaded: false,
              fav: false,
            };
          },
          methods: {
            toggleFav() {
              if (this.fav) {
                this.deleteFav();
              } else {
                this.addFav();
              }
            },
            mainClasses() {
              return ['favorite--' + this.data.view_mode, 'favorite--' + this.data.layout];
            },
            buttonClasses() {
              let layoutClass = 'favorite__btn--' + this.data.layout;
              return {
                'favorite__btn--remove': this.fav,
                [layoutClass]: true,
              };
            },
            addFav() {
              this.fav = true;
              this.like_clicked = true;
              var favorites = this.$cookies.get('favorites');
              if (typeof favorites !== 'object' || favorites == null) {
                favorites = {};
              }
              favorites[this.data.product_id] = true;
              this.$cookies.set('favorites', favorites, '30d');
              var total = Object.keys(favorites).length;
              this.$store.commit('updateFavorite', { total });
            },
            // Удалить из избранное.
            deleteFav() {
              this.fav = false;
              this.like_clicked = false;
              var favorites = this.$cookies.get('favorites');
              if (typeof favorites !== 'object' || favorites == null) {
                favorites = {};
              }
              delete favorites[this.data.product_id];
              this.$cookies.set('favorites', favorites, '30d');
              if (window.location.pathname == '/favorites') {
                window.location.reload();
              } else {
                var total = Object.keys(favorites).length;
                this.$store.commit('updateFavorite', { total });
              }
            },
          },
          created() {
            this.products_loaded = true;
            let favorites = this.$cookies.get('favorites');
            if (favorites !== null && typeof favorites == 'object' && favorites.hasOwnProperty(this.data.product_id)) {
              this.fav = true;
            }
            if (localStorage.watched) {
              var array = localStorage.watched.split('|');
            } else {
              var array = [];
            }
            let index = array.indexOf(this.data.product_id.toString());
            if (index === -1) {
              array.push(this.data.product_id);
              localStorage.watched = array.join('|');
            }
          },
        });
      }
      if (comp_template || comp_template_fav) {
        app.mount(el);
      }
    });
  }

  String.prototype.replaceAll = function (search, replacement) {
    return this.split(search).join(replacement);
  };

  function siteReady() {
    const END = Date.now() + 3000;
    var body = document.body;

    var interval = setInterval(() => {
      if (body.classList.contains('site-is-ready')) {
        clearInterval(interval);
        var targetNodes = $('.catalog');
        var MutationObserver = window.MutationObserver || window.WebKitMutationObserver;
        var myObserver = new MutationObserver(mutationHandler);
        var obsConfig = {
          childList: true,
          characterData: true,
          attributes: true,
          subtree: true,
        };
        targetNodes.each(function () {
          myObserver.observe(this, obsConfig);
        });

        function mutationHandler(mutationRecords) {
          mutationRecords.forEach(function (mutation) {
            setTimeout(() => {
              vueCartApp('.product-teaser');
            }, 1000);
          });
        }
        return;
      }
      if (Date.now() > END) {
        body.classList.add('site-is-ready');
        clearInterval(interval);
      }
    }, 1000);

    window.onload = () => {
      body.classList.add('site-is-ready');
    };
  }
})(jQuery);
