/**
 * @file
 * Vue app for small cart.
 */

(function () {
  "use strict";

  if (document.readyState != 'loading') {
    vueCartApp("#cart-favorite-small");
  } else {
    window.addEventListener("DOMContentLoaded", function () {
      vueCartApp("#cart-favorite-small");
    });
  }

  function vueCartApp(sel) {
    "use strict";
    var app;
    const elements = document.querySelectorAll(sel);
    const each = Array.prototype.forEach;
    each.call(elements, function (el, i) {
      let comp_template = el.querySelector('cart-small');
      let comp_template_fav = el.querySelector('favorite-small');
      app = Vue.createApp({ delimiters, comments, el }).use(store).use($cookies);
      app.component("cart-small", {
        template: comp_template,
        delimiters: delimiters,
        data() {
          return {
            ready: false,
          };
        },
        computed: {
          cart() {
            return this.$store.state.cart;
          },
        },
        methods: {
          initCart() {
            let pathPrefix = drupalSettings.path.pathPrefix;
            let loadPath = "/" + pathPrefix + "api/cart-load";
            axios.get(loadPath).then(
              (response) => {
                this.ready = true;
                this.$store.commit("updateCart", response.data);
              }
            );
          },
        },
        created() {
          setTimeout(() => {
            this.initCart();
          }, 10);
        },
      });
      app.component("favorite-small", {
        template: comp_template_fav,
        delimiters: delimiters,
        data() {
          return {
            ready: false,
          };
        },
        computed: {
          total() {
            return this.$store.state.favorite.total;
          },
        },
        methods: {
          initFavorite() {
            var favorites = this.$cookies.get("favorites");
            if (typeof favorites !== "object" || favorites == null) {
              favorites = {};
            }
            let total = Object.keys(favorites).length;
            this.$store.commit("updateFavorite", { total });
            this.ready = true;
          },
        },
        created() {
          setTimeout(() => {
            this.initFavorite();
          }, 10);
        },
      });
      app.mount(el);
    });
  }

})();
