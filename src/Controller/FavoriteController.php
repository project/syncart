<?php

namespace Drupal\syncart\Controller;

use Drupal\commerce_price\Price;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;

/**
 * Favorite Controller.
 */
class FavoriteController extends ControllerBase {

  /**
   * Constructs a new FavoriteController object.
   */
  public function __construct() {
    $this->renderer = \Drupal::service('renderer');
    $this->entityTypeManager = \Drupal::entityTypeManager();
    $this->productStorage = $this->entityTypeManager->getStorage('commerce_product');
    $this->viewBuilder = $this->entityTypeManager->getViewBuilder('commerce_product');
    $this->products = [];
    $this->output = [
      'products' => [],
      'information' => [],
    ];
  }

  /**
   * Favorites page.
   */
  public function favoritesPage(Request $request) {
    $this->request = $request;
    \Drupal::service('page_cache_kill_switch')->trigger();
    $this->getFavoritesProducts();
    $this->getFavoritesInformation();
    $config = \Drupal::config('synlanding.settings');
    if ($config) {
      $this->output['layout'] = $config->get('design');
    }
    return [
      '#theme' => 'syncart-favorites',
      '#data' => $this->output,
      '#cache' => [
        'max-age' => 0,
      ],
    ];
  }

  /**
   * Get favorites products.
   */
  private function getFavoritesProducts() {
    $favorites = $this->getFavoritesIds();
    if ($favorites) {
      $this->products = $this->productStorage->loadMultiple($favorites);
      $this->output['products'] = $this->makeProductsTeaser();
    }
  }

  /**
   * Get favorites information.
   */
  private function getFavoritesInformation() {
    $this->output['information'] = [
      'count' => count($this->output['products']),
      'price' => 0,
      'ids' => [],
    ];
    $this->getVariationsInformation();
  }

  /**
   * Get product ids.
   */
  private function getFavoritesIds() {
    $favorites = [];

    $fav = $this->request->cookies->get('favorites');
    if (empty($fav)) {
      return [];
    }

    if (strlen($fav) > 2) {
      $favorites = Json::decode($fav);
    }

    if (is_array($favorites)) {
      $favorites = array_keys($favorites);
    }

    return $favorites;
  }

  /**
   * Get total price.
   */
  private function getVariationsInformation() {
    $price = 0;
    $ids = [];
    $currency = "";
    $this->output['information']['price'] = 0;
    foreach ($this->products as $product) {
      $field = $product->variations;
      if ($field->isEmpty()) {
        continue;
      }
      $variation = $field->first()->entity;
      if (!is_object($variation)) {
        continue;
      }
      $price += (float) $variation->getPrice()->getNumber();
      $currency = $variation->getPrice()->getCurrencyCode();
      $ids[] = $variation->id();
    }
    if ($price > 0) {
      $this->output['information']['price'] = [
        '#type' => 'inline_template',
        '#template' => '{{ price|commerce_price_format }}',
        '#context' => [
          'price' => new Price($price, $currency),
        ],
      ];
    }
    $this->output['information']['ids'] = Json::encode($ids);
  }

  /**
   * Make products teaser.
   */
  private function makeProductsTeaser() {
    $output = [];
    foreach ($this->products as $product) {
      $build = $this->viewBuilder->view($product, 'teaser');
      $output[] = $this->renderer->render($build);
    }
    return $output;
  }

}
