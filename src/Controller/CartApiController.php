<?php

namespace Drupal\syncart\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\syncart\Service\SynRenderServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Cart JSON-DATA: only load cart info.
 */
class CartApiController extends ControllerBase {

  /**
   * Constructs a new CartController object.
   *
   * @param \Drupal\syncart\Service\SynRenderServiceInterface $syncart_renderer
   *   Syncart Renderer.
   */
  public function __construct(
    SynRenderServiceInterface $syncart_renderer
  ) {
    $this->renderer = $syncart_renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('syncart.render')
    );
  }

  /**
   * Debug page.
   */
  public function debug() {
    return [];
  }

  /**
   * Cart.
   */
  public function cart() {
    return [
      '#theme' => 'syncart-cart',
      '#data' => [
        'cart' => $this->renderer->data(),
      ],
      '#cache' => [
        'max-age' => 0,
      ],
    ];
  }

  /**
   * Получение текущей корзины.
   */
  public function loadCart($cid = FALSE) {
    return new JsonResponse($this->renderer->data($cid));
  }

}
