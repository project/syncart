<?php

namespace Drupal\syncart\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Admin page Controller.
 */
class AdminController extends ControllerBase {

  /**
   *
   */
  public function page() {
    $rows = [];
    $syncart_admin = \Drupal::service('syncart.admin');
    $order_types = $syncart_admin->getOrderTypes();
    foreach ($order_types as $order_type_id => $order_type) {
      $number_pattern = $order_type->getNumberPattern();
      $number_pattern_id = $number_pattern->id();
      $rows[] = [
        $order_type_id,
        $number_pattern_id,
        $syncart_admin->getNumberPatternPattern($number_pattern_id),
        $syncart_admin->getNumberPatternSequence($number_pattern_id),
        $syncart_admin->getNumberPatternInitialNumber($number_pattern_id),
        [
          'data' => ['#markup' => "<a href='/admin/config/syncart/flush_sequence/$number_pattern_id' class='button button--primary'>Сбросить</a>"],
        ],
      ];
    }
    return [
      'flush_title' => ['#markup' => "<h2>Сбросить номера заказов</h2>"],
      'flush_table' => [
        '#type' => 'table',
        '#header' => ['Тип заказа', 'Шаблон номера', 'Паттерн', 'Актуальный номер', 'Начальный номер', 'Сброс'],
        '#rows' => $rows,
        '#attributes' => ['class' => ['table', 'table-striped', 'table-hover']],
        '#allowed_tags' => ['p', 'h2', 'small', 'br'],
      ],
    ];
  }

  /**
   *
   */
  public function flushSequence($number_pattern_id) {
    \Drupal::service('syncart.admin')->resetNumberPatternSequence($number_pattern_id);
    return $this->redirect('syncart.admin.page');
  }

}
