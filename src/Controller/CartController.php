<?php

namespace Drupal\syncart\Controller;

use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_price\Price;
use Drupal\commerce_product\Entity\ProductInterface;
use Drupal\commerce_product\Entity\ProductVariationInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\syncart\Service\SynCartServiceInterface;
use Drupal\syncart\Service\SynRenderServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Cart Controller.
 */
class CartController extends ControllerBase {

  const CART_ROUTE = 'syncart.cart_controller_cart';

  /**
   * Current cart.
   *
   * @var \Drupal\syncart\Service\SynCart
   */
  protected $cart;

  /**
   * Cart Renderer.
   *
   * @var \Drupal\syncart\Service\SynRenderService
   */
  protected $renderer;

  /**
   * Constructs a new CartController object.
   *
   * @param \Drupal\syncart\Service\SynCartServiceInterface $syncart
   *   Syncart cart.
   * @param \Drupal\syncart\Service\SynRenderServiceInterface $syncart_renderer
   *   Syncart Renderer.
   */
  public function __construct(
    SynCartServiceInterface $syncart,
    SynRenderServiceInterface $syncart_renderer
  ) {
    $this->cart = $syncart;
    $this->renderer = $syncart_renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('syncart.cart'),
      $container->get('syncart.render')
    );
  }

  /**
   * Cart.
   */
  public function checkout() {
    if ($this->cart->isEmpty()) {
      return $this->redirect('syncart.cart_controller_cart');
    }
    $cart = $this->cart->load();
    $url = Url::fromRoute('commerce_checkout.form', [
      'commerce_order' => $cart->id(),
      'step' => 'order_information',
    ]);
    return new RedirectResponse($url->toString());
  }

  /**
   * Add product variation to cart.
   */
  public function addToCart(Request $request) {
    $params = Json::decode(
      $request->getContent()
    );
    if (\Drupal::config('syncart.settings')->get('donation')) {
      /** @var \Drupal\commerce_product\Entity\ProductVariationInterface $variation */
      $variation = \Drupal::entityTypeManager()
        ->getStorage('commerce_product_variation')
        ->load($params['vid']);
      $currency_code = $variation->getPrice()->getCurrencyCode();
      $product = $variation->getProduct();
      $variation = $this->getRightVariation($product, $params['donation']);
      if (empty($variation)) {
        $variation = $this->createVariation($product, $params['donation'], $currency_code);
      }
      $params['vid'] = $variation->id();
      $params['quantity'] = 1;
    }
    if (!empty($params['vid'])) {
      $this->cart->addToCart([
        'vid' => $params['vid'],
        'quantity' => $params['quantity'],
        'selected_products' => $params['selected_products'] ?? NULL,
      ]);
    }
    return new JsonResponse($this->renderer->data());
  }

  /**
   * Add variations to cart.
   */
  public function addToCartMultiple(Request $request) {
    $params = Json::decode(
      $request->getContent()
    );
    if (!empty($params['vids'])) {
      foreach ($params['vids'] as $vid) {
        $this->cart->addToCart(['vid' => $vid, 'quantity' => 1]);
      }
    }
    return new JsonResponse($this->renderer->data());
  }

  /**
   * Проверка доступности необходимого числа товаров на складе.
   */
  public function refreshStock(Request $request) {
    $result = [];
    $params = Json::decode($request->getContent());
    if (isset($params['items'])) {
      foreach ($params['items'] as $order_item_id => $item_data) {
        $stock = $this->cart->getOrderItemStock($item_data['vid']);
        $result[] = [
          'item_id' => $order_item_id,
          'vid' => $item_data['vid'],
          'stock' => $stock,
        ];
      }
    }
    return new JsonResponse($result);
  }

  /**
   * Задать количество товара в корзине.
   */
  public function setVariationQuantity(Request $request) {
    $content_parameters = Json::decode(
      $request->getContent()
    );
    $this->cart->addToCart($content_parameters);
    return new JsonResponse($this->renderer->data());
  }

  /**
   * Задать количество для позиции заказа.
   */
  public function setOrderItemQuantity(Request $request) {
    $content_parameters = Json::decode(
      $request->getContent()
    );

    if (isset($content_parameters['item_id']) && (int) $content_parameters['item_id'] != 0) {
      if (isset($content_parameters['quantity']) && (int) $content_parameters['quantity'] != 0) {
        $order_item_id = (int) $content_parameters['item_id'];
        $quantity = round($content_parameters['quantity'], 2);
        $this->cart->setOrderItemQuantity($order_item_id, $quantity);
      }
    }
    return new JsonResponse($this->renderer->data());
  }

  /**
   * Удалить товар в корзине.
   */
  public function removeFromCart(Request $request) {
    $content_parameters = Json::decode(
      $request->getContent()
    );
    if (!empty($content_parameters['item_id'])) {
      $this->cart->removeOrderItem($content_parameters['item_id']);
    }
    return $this->redirect(self::CART_ROUTE);
  }

  /**
   * Задать примечание к позиции заказа.
   */
  public function setItemNote(Request $request) {
    $params = Json::decode($request->getContent());
    if (!empty($params['itemId'])) {
      $this->cart->setOrderItemNote($params['itemId'], $params['note']);
    }
    return new JsonResponse($this->renderer->data());
  }

  /**
   * Получение текущей корзины.
   */
  public function loadCart($cid = FALSE) {
    return new JsonResponse($this->renderer->data($cid));
  }

  /**
   * {@inheritdoc}
   */
  public function repeatOrder(Order $commerce_order) {
    $this->cart->addOrderItem($commerce_order);
    return $this->redirect('syncart.cart_controller_cart');
  }

  /**
   * {@inheritdoc}
   */
  private function getRightVariation(ProductInterface $product, Int $amount) :? ProductVariationInterface {
    foreach ($product->getVariations() as $variation) {
      if ($variation->getPrice()->getNumber() == $amount) {
        return $variation;
      }
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  private function createVariation(ProductInterface $product, Int $amount, $currency_code) :? ProductVariationInterface {
    $variation = \Drupal::entityTypeManager()->getStorage('commerce_product_variation')
      ->create([
        'type' => 'variation',
        'product_id' => $product->id(),
        'price' => new Price($amount, $currency_code),
      ]);
    $variation->save();
    return $variation;
  }

}
