<?php

namespace Drupal\syncart\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements the form controller.
 */
class Settings extends ConfigFormBase {
  /**
   * AJAX Wrapper.
   *
   * @var wrapper
   */

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'syncart';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['syncart.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('syncart.settings');
    $form['check_store'] = [
      '#title' => $this->t('Consider stock availability'),
      '#description' => '',
      '#type' => 'checkbox',
      '#required' => FALSE,
      '#default_value' => $config->get('check_store'),
    ];
    $form['no_variation_text'] = [
      '#title' => $this->t('Text for the product without variations'),
      '#description' => '',
      '#type' => 'textfield',
      '#size' => 60,
      '#maxlength' => 128,
      '#required' => FALSE,
      '#default_value' => $config->get('no_variation_text'),
    ];
    $form['note'] = [
      '#title' => $this->t('Order item note'),
      '#description' => '',
      '#type' => 'checkbox',
      '#required' => FALSE,
      '#default_value' => $config->get('note'),
    ];
    $form['donation'] = [
      '#title' => $this->t('Used as donation'),
      '#description' => '',
      '#type' => 'checkbox',
      '#required' => FALSE,
      '#default_value' => $config->get('donation'),
    ];
    $form['registration'] = [
      '#title' => $this->t('User registration'),
      '#description' => '',
      '#type' => 'checkbox',
      '#required' => FALSE,
      '#default_value' => $config->get('registration'),
    ];
    $form['registration_fields'] = [
      '#title' => "{$this->t('User registration Form Fields')} ({$this->t('Not use')})",
      '#description' => '',
      '#type' => 'checkbox',
      '#required' => FALSE,
      '#default_value' => $config->get('registration_fields'),
    ];
    $form['show_quantity'] = [
      '#title' => $this->t('Show quantity selection on add to cart'),
      '#description' => '',
      '#type' => 'checkbox',
      '#required' => FALSE,
      '#default_value' => $config->get('show_quantity'),
      '#attributes' => [
        'name' => 'show_quantity',
      ],
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * Implements a form submit handler.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('syncart.settings');
    $cart = \Drupal::service('syncart.cart');
    $cart->updateUserRegister($form_state->getValue('registration'));
    $config
      ->set('check_store', $form_state->getValue('check_store'))
      ->set('cart_time_limit', $form_state->getValue('cart_time_limit'))
      ->set('no_variation_text', $form_state->getValue('no_variation_text'))
      ->set('show_quantity', $form_state->getValue('show_quantity'))
      ->set('registration', $form_state->getValue('registration'))
      ->set('registration_fields', $form_state->getValue('registration_fields'))
      ->set('donation', $form_state->getValue('donation'))
      ->set('note', $form_state->getValue('note'))
      ->save();
  }

}
