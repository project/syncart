<?php

namespace Drupal\syncart\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Cart' Block.
 *
 * @Block(
 *   id = "small_cart_block",
 *   admin_label = @Translation("Small cart block"),
 *   category = @Translation("Small cart block"),
 * )
 */
class CartBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    ModuleHandlerInterface $module_handler,
    ConfigFactoryInterface $config_factory
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->moduleHandler = $module_handler;
    $this->configFactory = $config_factory;
  }

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   *
   * @return static
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('module_handler'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Synlanding config.
    if ($this->moduleHandler->moduleExists('synlanding')) {
      $synlanding_path = $this->moduleHandler->getModule('synlanding')->getPath();
      $design = $this->configFactory->get('synlanding.settings')->get('design');
    }
    return [
      '#theme' => 'syncart-cart-small',
      '#data' => [
        'design' => $design ?? NULL,
        'syncart' => $this->moduleHandler->getModule('syncart')->getPath(),
        'synlanding' => $synlanding_path ?? NULL,
        'cart' => [],
        'allowFavorite' => $this->moduleHandler->moduleExists('flag'),
      ],
    ];
  }

}
