<?php

namespace Drupal\syncart\Plugin\Commerce\CheckoutPane;

use Drupal\commerce_checkout\Event\CheckoutCompletionRegisterEvent;
use Drupal\commerce_checkout\Event\CheckoutEvents;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CompletionRegister;
use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\user\UserInterface;

/**
 * Provides the registration after checkout pane.
 *
 * @CommerceCheckoutPane(
 *   id = "syncart_finalaze",
 *   label = @Translation("Syncart registration"),
 *   display_label = @Translation("Account finalize"),
 *   default_step = "complete",
 * )
 */
class SyncartFinalize extends CompletionRegister {

  /**
   * {@inheritdoc}
   */
  public function isVisible() {
    // OLD: return $this->currentUser->isAnonymous();
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function buildPaneForm(array $pane_form, FormStateInterface $form_state, array &$complete_form) {
    $config = \Drupal::config('syncart.settings');
    $registration = $config->get('registration');
    $user = \Drupal::currentUser();
    $uid = \Drupal::currentUser()->id();
    $order_type = $this->order->type->target_id;
    $email = $this->order->getEmail();
    $profile = $this->order->billing_profile->entity;
    $profileEmail = $profile->field_customer_email->value;
    if ($order_type == 'pos') {
      if ($user && $uid != 1) {
        user_logout();
      }
    }
    else {
      if (!$uid || ($uid && $email != $profileEmail)) {
        $user = $this->getUserEmail($profileEmail);
        if ($user) {
          $do_login_form = TRUE;
          $form_state->set('login_name', $user->name->value);
          $pane_form['my_orders'] = [
            '#markup' => $this->t('<a href=":link">Login</a> to view your orders.', [
              ':link' => "/user/login",
            ]),
          ];
        }
        else {
          if ($profileEmail) {
            $user = $this->userCreate($profileEmail, $registration);
            if ($config->get('registration_fields')) {
              // @todo Check this staff.
              $do_create_form = TRUE;
              $form_state->set('created_user', $user);
            }
            else {
              if (!$uid) {
                user_login_finalize($user);
              }
            }
            if ($registration) {
              $this->userRegisterEvent($pane_form, $form_state, $user);
              _user_mail_notify('register_no_approval_required', $user);
            }
            else {
              // @todo [Зачем?] отправили письмо с одноразоввой ссылкой входа
              // _user_mail_notify('status_activated', $user);
              // _user_mail_notify('register_no_approval_required', $user);
              $pane_form['new'] = [
                '#markup' => $this->t('We send order data to %email.', [
                  '%email' => $profileEmail,
                ]),
              ];
            }
          }
        }

        if ($user) {
          // Finish order -> cart.
          $this->orderAssignment->assign($this->order, $user);
          // From Extra Fields.
          if (!empty($do_login_form) && $user->status->value) {
            $this->formExistingUserPassword($pane_form);
          }
          elseif (!empty($do_create_form)) {
            $this->formNewUserFields($pane_form, $form_state);
          }
        }
      }

    }
    return $pane_form;
  }

  /**
   * Create New User from PROFILE.
   */
  private function userCreate($email, $registration) {
    $profile = $this->order->billing_profile->entity;
    $nameEmail = strstr($email, '@', TRUE) ?? 'No e-mail';
    $name = $profile->field_customer_name->value;
    $surname = $profile->field_customer_surname->value;
    // Add some entropy to name.
    $hash = substr(sha1($this->order->uuid->value), 0, 8);
    $username = mb_substr($nameEmail, 0, UserInterface::USERNAME_MAX_LENGTH - 15);
    $usr = [
      'mail' => $email,
      'status' => $registration ? TRUE : FALSE,
      'name' => "$username-$hash",
      'field_user_name' => $name,
      'field_user_surname' => $surname,
      'field_user_phone' => $profile->field_customer_phone->value,
    ];
    $user = $this->userStorage->create($usr);
    $user->enforceIsNew();
    $user->save();
    return $user;
  }

  /**
   * Event: User Register.
   */
  private function userRegisterEvent(&$pane_form, $form_state, $user) {
    $email = $user->mail->value;
    $message = $this->t('Registration successful. You are now logged in.');
    $this->messenger()->addStatus($message);
    $uid = $user->id();
    // $this->messenger()->addStatus(" - Мои заказы");
    $pane_form['new'] = [
      '#markup' => " $email",
    ];
    $pane_form['new'] = [
      '#markup' => $this->t('New user with %email created.', [
        '%email' => $email,
      ]),
    ];
    $pane_form['my_orders'] = [
      '#markup' => $this->t('<a href=":link">My Orders</a>.', [
        ':link' => "/user/$uid/orders",
      ]),
    ];
    // Notify other modules.
    $account = \Drupal::currentUser();
    $event = new CheckoutCompletionRegisterEvent($account, $this->order);
    $this->eventDispatcher->dispatch($event, CheckoutEvents::COMPLETION_REGISTER);
    if ($url = $event->getRedirectUrl()) {
      $form_state->setRedirectUrl($url);
    }
  }

  /**
   * Form with Pass for existing user login.
   */
  private function formExistingUserPassword(&$pane_form) {
    $pane_form['do_login'] = [
      '#type' => 'hidden',
      '#value' => TRUE,
    ];
    $pane_form['pass'] = [
      '#title' => $this->t('Password'),
      '#type' => 'password',
      '#size' => 60,
      '#description' => $this->t('Password for @mail.'),
      '#required' => TRUE,
    ];
    $pane_form['actions'] = [
      '#type' => 'actions',
      'login' => [
        '#type' => 'submit',
        '#name' => 'checkout_completion_register',
        '#value' => $this->t('Login'),
      ],
    ];
  }

  /**
   * Form `display=register` for NEW user.
   */
  private function formNewUserFields(&$pane_form, $form_state) {
    $pane_form['do_create'] = [
      '#type' => 'hidden',
      '#value' => TRUE,
    ];
    $pane_form['actions'] = [
      '#type' => 'actions',
      'register' => [
        '#type' => 'submit',
        '#value' => $this->t('Create account & Login'),
        '#name' => 'checkout_completion_register',
      ],
    ];
    /** @var \Drupal\user\UserInterface $account */
    $account = $this->entityTypeManager->getStorage('user')->create([]);
    $form_display = EntityFormDisplay::collectRenderDisplay($account, 'register');
    $form_display->buildForm($account, $pane_form, $form_state);
  }

  /**
   * Load User by mail.
   */
  private function getUserEmail($email) {
    if (empty($email)) {
      return FALSE;
    }
    $uids = $this->userStorage->getQuery()
      ->condition('mail', $email)
      // ->condition('status', 1)
      ->accessCheck(TRUE)
      ->execute();
    if (empty($uids)) {
      return FALSE;
    }
    $uid = array_shift($uids);
    return $this->userStorage->load($uid);
  }

  /**
   * {@inheritdoc}
   */
  public function validatePaneForm(array &$pane_form, FormStateInterface $form_state, array &$complete_form) {
    $values = $form_state->getValue($pane_form['#parents']);
    if ($values['do_login']) {
      // @todo $username;
      $username = $form_state->get('login_name');
      $password = trim($values['pass']);
      $name_element = $pane_form['pass'];

      $uid = $this->userAuth->authenticate($username, $password);
      // Generate the "reset password" url.
      $query = !empty($username) ? ['name' => $username] : [];
      $password_url = Url::fromRoute('user.pass', [], ['query' => $query])->toString();
      if (empty($username) || empty($password)) {
        $msg = $this->t('Unrecognized username or password. <a href=":url">Have you forgotten your password?</a>', [
          ':url' => $password_url,
        ]);
        $form_state->setError($pane_form['pass'], $msg);
        return;
      }
      if (!$uid) {
        $this->credentialsCheckFlood->register($this->clientIp, $username);
        $msg = $this->t('Unrecognized username or password. <a href=":url">Have you forgotten your password?</a>', [
          ':url' => $password_url,
        ]);
        $form_state->setError($name_element, $msg);
      }
      $form_state->set('logged_in_uid', $uid);
    }
    elseif ($values['do_create']) {
      $account = $form_state->get('created_user');
      /** @var \Drupal\user\UserInterface $account */
      $form_display = EntityFormDisplay::collectRenderDisplay($account, 'register');
      $form_display->extractFormValues($account, $pane_form, $form_state);
      $form_display->validateFormValues($account, $pane_form, $form_state);
      if (!$form_state->hasAnyErrors()) {
        user_login_finalize($account);
        $account->save();
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitPaneForm(array &$pane_form, FormStateInterface $form_state, array &$complete_form) {
    $values = $form_state->getValue($pane_form['#parents']);
    if ($values['do_login']) {
      $storage = $this->entityTypeManager->getStorage('user');
      /** @var \Drupal\user\UserInterface $account */
      $account = $storage->load($form_state->get('logged_in_uid'));
      user_login_finalize($account);
      $this->order->setCustomer($account);
      $this->credentialsCheckFlood->clearAccount($this->clientIp, $account->getAccountName());

      $form_state->setRedirect('commerce_checkout.form', [
        'commerce_order' => $this->order->id(),
        'step' => $this->checkoutFlow->getNextStepId($this->getStepId()),
      ]);
    }
    elseif ($values['do_create']) {
      // Is OK on validate.
    }

  }

}
