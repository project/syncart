<?php

namespace Drupal\syncart\Event;

/**
 * Events dispatched by this module.
 *
 * @package Drupal\commerce_checkout_link\Event
 */
final class CommerceCheckoutLinkEvents {

  /**
   * Name of the event fired when redirecting to checkout via link.
   *
   * @Event
   *
   * @see \Drupal\syncart\Event\CheckoutLinkEvent
   */
  const CHECKOUT_LINK_REDIRECT = 'syncart.redirect';

}
