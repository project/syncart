<?php

namespace Drupal\syncart\Service;

use Drupal\commerce_cart\CartProviderInterface;
use Drupal\commerce_order\Entity\OrderItemInterface;
use Drupal\commerce_price\CurrencyFormatter;
use Drupal\commerce_price\Price;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\image\Entity\ImageStyle;
use Drupal\taxonomy\Entity\Term;

/**
 * Custom Cart Service.
 */
class SynRenderService implements SynRenderServiceInterface {

  const ORDER_ITEM_BUNDLE_PRODUCT_FEATURE = 'product_feature';

  /**
   * Провайдер корзины.
   *
   * @var \Drupal\commerce_cart\CartProviderInterface
   */
  protected $cartProvider;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The Currency formatter.
   *
   * @var \Drupal\commerce_price\CurrencyFormatter
   */
  protected $currencyFormatter;

  /**
   * The object renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a new SynCartService object.
   *
   * @param \Drupal\commerce_cart\CartProviderInterface $cart_provider
   *   Провайдер корзины.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\commerce_price\CurrencyFormatter $currency_formatter
   *   The currency formatter.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The object renderer.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(
    CartProviderInterface $cart_provider,
    EntityTypeManagerInterface $entity_type_manager,
    CurrencyFormatter $currency_formatter,
    RendererInterface $renderer,
    ConfigFactoryInterface $config_factory
  ) {
    $this->cartProvider = $cart_provider;
    $this->entityTypeManager = $entity_type_manager;
    $this->currencyFormatter = $currency_formatter;
    $this->renderer = $renderer;
    $this->configFactory = $config_factory;
  }

  /**
   * Cart Info.
   */
  public function data($cid = FALSE, $render = []) {
    $cart = $this->getCart($cid);

    $qitems = 0;
    $render = [
      'qitems' => 0,
      'price' => 0,
      'number' => 0,
      'quantity' => 0,
    ];
    if ($cart) {
      /** @var \Drupal\commerce_order\OrderStorageInterface $cart */
      $promotions_sum = 0;
      $items = $cart->getItems();
      $render['id'] = $cart->id();
      $config = $this->configFactory->get('syncart.settings');
      $render['note'] = (bool) $config->get('note') ?? FALSE;
      $unsorted_items = [];
      $items_data = [];
      foreach ($items as $index => $order_item) {
        $qitems++;
        $variation_info = $this->getVariationInfo($order_item);
        if (empty($variation_info)) {
          continue;
        }
        if (!empty($variation_info['data'])) {
          $items_data[] = $variation_info['id'];
        }
        $unsorted_items[$order_item->id()] = $variation_info;
        $render['quantity'] += $order_item->getQuantity();
        if (!empty($variation_info['adjustments'])) {
          foreach ($variation_info['adjustments'] as $adjustment) {
            if ($adjustment['type'] == 'promotion') {
              $promotions_sum += $adjustment['number'];
            }
          }
        }
      }
      $groups = [];
      $sorted_items = [];
      if (!empty($items_data)) {
        foreach ($items_data as $group) {
          $groups[] = $group;
          $unsorted_items[$group]['group'] = $group;
          $sorted_items[] = $unsorted_items[$group];
          foreach ($unsorted_items[$group]['data'][0] as $key => $quantity) {
            $temp_data = explode(':', $key);
            if ($temp_data[0] == 'promotion') {
              $promotion = \Drupal::entityTypeManager()->getStorage('commerce_promotion')->load($temp_data[1]);
              $configuration = $promotion->getOffer()->getConfiguration();
              foreach ($configuration['buy_conditions'][0]['configuration']['products'] as $productArray) {
                $products = \Drupal::entityTypeManager()->getStorage('commerce_product')->loadByProperties(['uuid' => $productArray['product']]);
                $product = array_keys($products);
                $pid = array_shift($product);
                foreach ($unsorted_items as $itemId => $item) {
                  if ($item['pid'] == $pid) {
                    $item['group'] = $group;
                    $sorted_items[] = $item;
                    unset($unsorted_items[$item['id']]);
                  }
                }
              }
            }
          }
          unset($unsorted_items[$group]);
        }
        $sorted_items = array_reverse($sorted_items);
      }
      foreach ($unsorted_items as $item) {
        $groups[] = $item['group'];
        $sorted_items[] = $item;
      }
      $render['groups'] = $groups;
      $render['items'] = $sorted_items;
      $adj_sum = 0;
      foreach ($cart->getAdjustments() as $index => $adjustment) {
        $adj_sum += $adjustment->getAmount()->getNumber();
        $adj_price = [
          '#type' => 'inline_template',
          '#template' => '{{ price|commerce_price_format }}',
          '#context' => [
            'price' => $adjustment->getAmount(),
          ],
        ];
        $adj_price_renderable = $this->renderer->renderRoot($adj_price);
        $render['adjustments'][$index] = [
          'label' => $adjustment->getLabel(),
          'type' => $adjustment->getType(),
          'source' => $adjustment->getSourceId(),
          'is_included' => $adjustment->isIncluded(),
          'price' => $adj_price_renderable,
        ];
      }
      if (!empty($items)) {
        $total = [
          '#type' => 'inline_template',
          '#template' => '{{ price|commerce_price_format }}',
          '#context' => [
            'price' => $cart->getTotalPrice(),
          ],
        ];
        $subtotal = [
          '#type' => 'inline_template',
          '#template' => '{{ price|commerce_price_format }}',
          '#context' => [
            'price' => $cart->getSubtotalPrice(),
          ],
        ];
        $price_sum = 0;
        foreach ($cart->getItems() as $item) {
          $variation = $item->getPurchasedEntity();
          $price_sum += $variation->getPrice()->getNumber() * $item->getQuantity();
        };
        $product_feature_amount = $this->getProductFeatureAmount();
        $full_price = new Price($price_sum + $product_feature_amount, $cart->getSubtotalPrice()->getCurrencyCode());
        $full_price = [
          '#type' => 'inline_template',
          '#template' => '{{ price|commerce_price_format }}',
          '#context' => [
            'price' => $full_price,
          ],
        ];
        $render['promotion'] = $promotions_sum;
        $promotion = new Price($promotions_sum, $cart->getSubtotalPrice()->getCurrencyCode());
        $promotion = [
          '#type' => 'inline_template',
          '#template' => '{{ price|commerce_price_format }}',
          '#context' => [
            'price' => $promotion,
          ],
        ];
        $render['promosum'] = $this->renderer->renderRoot($promotion);
        $render['qitems'] = $qitems;
        $subtotal_renderable = $this->renderer->renderRoot($subtotal);
        $render['price'] = $subtotal_renderable;
        $render['subtotal'] = $subtotal_renderable;
        $render['total'] = $this->renderer->renderRoot($total);
        $render['full_price'] = $this->renderer->renderRoot($full_price);
        $render['number'] = $cart->getSubtotalPrice()->getNumber();
      }
    }
    return $render;
  }

  /**
   * Get subtotal price.
   */
  public function getProductFeatureAmount() : float {
    $amount = 0;
    if ($adjustments = $this->cart->collectAdjustments(['syncart_product_feature'])) {
      foreach ($adjustments as $adjustment) {
        $adjustment_amount = (float) $adjustment->getAmount()->getNumber();
        $amount += $adjustment_amount;
      }
    }
    return $amount;
  }

  /**
   * Cart GET.
   */
  private function getCart($cid) {
    $cart = NULL;
    if ($cid) {
      $carts = $this->cartProvider->getCarts();
      if (count($carts)) {
        foreach ($carts as $key => $user_cart) {
          if ($user_cart->id() == $cid) {
            $cart = $user_cart;
          }
        }
      }
      else {
        $storage = \Drupal::entityTypeManager()->getStorage('commerce_order');
        $cart = $storage->load($cid);
      }
    }
    else {
      $order_type = \Drupal::service('syncart.cart')->getOrderType();
      $cart = $this->cartProvider->getCart($order_type);
    }
    $this->cart = $cart;
    return $cart;
  }

  /**
   * Получить информацию о вариации из товара в корзине.
   */
  public function getVariationInfo(OrderItemInterface $order_item) : array {
    $variation = $order_item->getPurchasedEntity();
    if (empty($variation)) {
      return [];
    }

    $info = [
      'id' => (int) $order_item->id(),
      'group' => $order_item->id(),
      'checkstore' => $this->getOrderItemCheckstore($order_item),
      'adjustments' => $this->getOrderItemAdjustments($order_item),
      'quantity' => round($order_item->getQuantity(), 2),
      'stock' => $this->getOrderItemStock($order_item),
      'number' => $variation->getPrice()->getNumber(),
      'currency' => $variation->getPrice()->getCurrencyCode(),
      'price' => $this->getOrderItemPrice($order_item),
      'price_number' => $variation->getPrice()->getNumber(),
      'totalPrice' => $this->getOrderItemTotalPrice($order_item),
      'selected_feature_products' => $this->getSelectedFeatureProducts($order_item),
    ];

    $this->getVariationProductInfo($variation, $info);
    $this->appendOrderItemData($info, $order_item);

    \Drupal::moduleHandler()->alter('syncart_variation', $info, $variation);

    return $info;
  }

  /**
   * Get stock.
   */
  private function getOrderItemCheckstore(OrderItemInterface $order_item) : bool {
    return (bool) $this->configFactory->get('syncart.settings')
      ->get('check_store');
  }

  /**
   * Get stock.
   */
  private function getOrderItemStock(OrderItemInterface $order_item) :? int {
    $checkstore = $this->getOrderItemCheckstore($order_item);
    if (!$checkstore) {
      return NULL;
    }
    $variation = $order_item->getPurchasedEntity();
    if (!$variation->hasField('field_stock')) {
      return NULL;
    }
    return intval($variation->field_stock->value);
  }

  /**
   * Get Features.
   */
  private function getSelectedFeatureProducts(OrderItemInterface $order_item) : array {
    $order_item_bundle = $order_item->bundle();
    if ($order_item_bundle != self::ORDER_ITEM_BUNDLE_PRODUCT_FEATURE) {
      return [];
    }
    elseif (!$order_item->hasField('field_json')) {
      return [];
    }
    return Json::decode($order_item->field_json->value ?? '[]');
  }

  /**
   * Get price.
   */
  private function getOrderItemAdjustments(OrderItemInterface $order_item) : array {
    $order_item_adjustments = $order_item->getAdjustments();
    if (empty($order_item_adjustments)) {
      return [];
    }
    $adjustments = [];
    foreach ($order_item_adjustments as $adjustment) {
      $adjustment_price = $adjustment->getAmount()->getNumber();
      $currency_code = $adjustment->getAmount()->getCurrencyCode();
      /** @var \Drupal\commerce_price\Entity\CurrencyInterface $currency */
      $currency = $this->entityTypeManager->getStorage('commerce_currency')
        ->load($currency_code);
      $adjustments[] = [
        'type' => $adjustment->getType(),
        'label' => $adjustment->getLabel(),
        'number' => $adjustment->getAmount()->getNumber(),
        'amount' => $this->currencyFormatter->format(
          $adjustment_price, $currency_code, []
        ),
        'currencyCode' => $currency_code,
        'currencySymbol' => $currency->getSymbol(),
        'percentage' => $adjustment->getPercentage(),
      ];
    }
    return $adjustments;
  }

  /**
   * Append order item data.
   */
  private function appendOrderItemData(
    array &$info,
    OrderItemInterface $order_item
  ) {
    $data = $order_item->data->getValue();
    $info['data'] = $data;
    foreach ($data as $dataValue) {
      foreach ($dataValue as $key => $value) {
        $info["data_$key"] = $value;
      }
    }
  }

  /**
   * Get price.
   */
  private function getOrderItemPrice(OrderItemInterface $order_item) : string {
    $variation = $order_item->getPurchasedEntity();
    $price = [
      '#type' => 'inline_template',
      '#template' => '{{ price|commerce_price_format }}',
      '#context' => [
        'price' => $variation->getPrice(),
      ],
    ];
    return $this->renderer->renderRoot($price);
  }

  /**
   * Get total.
   */
  private function getOrderItemTotalPrice(OrderItemInterface $order_item) : string {
    $total = [
      '#type' => 'inline_template',
      '#template' => '{{ price|commerce_price_format }}',
      '#context' => [
        'price' => $order_item->getTotalPrice(),
      ],
    ];
    return $this->renderer->renderRoot($total);
  }

  /**
   * Получить информацию о товаре вариации.
   */
  private function getVariationProductInfo($variation, &$info) {
    if (is_object($product = $variation->getProduct())) {
      if ($product->field_title && $product->field_title->value) {
        $title = $product->field_title->value;
      }
      if ($variation->field_variation_image) {
        $variationImage = $variation->field_variation_image->entity;
        if (is_object($variationImage)) {
          $uri = $variationImage->getFileUri();
          $image = ImageStyle::load('product_cart')->buildUrl($uri);
        }
      }

      $attributes = [];
      $attr = $variation->getAttributeValues();
      if ($attr) {
        foreach ($attr as $key => $value) {
          $attribute_item[] = [
            'id' => $value->id(),
            'label' => $value->getAttribute()->label(),
            'name' => $value->getName(),
          ];
        }
        if (!in_array($attribute_item, $attributes)) {
          $attributes[] = $attribute_item;
        }
      }

      $info['attributes'] = $attributes;
      $info['title'] = $title ?? $product->getTitle();
      if ($product->hasField('field_article')) {
        $article = $product->field_article->value;
      }
      $info['article'] = $article ?? '';
      if ($product->hasField('field_catalog')) {
        $catalog = $product->field_catalog->entity->getName();
      }
      $info['catalog'] = $catalog ?? '';
      $info['picture'] = $image ?? $this->getProductPicture($product);
      $info['url'] = $product->toUrl()->toString();
      $info['pid'] = (int) $product->id();
      $info['body'] = $product->body->value ?? '';
      $info['vid'] = (int) $variation->id();
      $uname = '';
      $qstep = 1;
      if ($product->hasField('field_unit')) {
        $unit = $product->get('field_unit')->getValue();
        if (!empty($unit)) {
          $term = Term::load($unit[0]['target_id']);
          $uname = $term->getName();
          if (!empty($term->field_unit_float->value) && (int) $term->field_unit_float->value != 0) {
            $config = $this->configFactory->get('core.entity_form_display.commerce_order_item.default.default');
            $qstep = round($config->getRawData()['content']['quantity']['settings']['step'], 2);
          }
        }
      }
      if ($product->hasField('field_package')) {
        $qstep = $product->field_package->value;
      }
      $info['qstep'] = (float) $qstep;
      $info['uname'] = $uname;
    }
  }

  /**
   * Загрузка изображения товара.
   */
  private function getProductPicture($product) {
    $image = '';
    $imageUri = '';
    $fieldImage = $product->field_image;
    $fieldGallery = $product->field_gallery;
    if ($fieldImage && !$fieldImage->isEmpty()) {
      foreach ($fieldImage as $item) {
        $imageUri = $item->entity->getFileUri();
        break;
      }
    }
    elseif ($fieldGallery && !$fieldGallery->isEmpty()) {
      foreach ($fieldGallery as $item) {
        $imageUri = $item->entity->getFileUri();
        break;
      }
    }
    if ($imageUri) {
      $image = ImageStyle::load('product_cart')->buildUrl($imageUri);
    }
    return $image;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $items = [];
    if (is_object($this->cart)) {
      $items = $this->cart->getItems();
    }
    return empty($items);
  }

}
