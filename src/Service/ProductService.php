<?php

namespace Drupal\syncart\Service;

use Drupal\commerce_order\Entity\OrderItemInterface;
use Drupal\commerce_product\Entity\ProductInterface;
use Drupal\commerce_product\Entity\ProductVariationInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\paragraphs\ParagraphInterface;

/**
 * Class Product Service.
 */
class ProductService {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Check stock availability.
   *
   * @var bool
   */
  protected $useCheckStoreAvailability;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    ConfigFactoryInterface $config_factory
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->configFactory = $config_factory;
    $this->initProcess();
  }

  /**
   * Get order_item Features.
   */
  public function getOrderItemFeatures(OrderItemInterface $order_item) : array {
    if (!$order_item->hasField('field_json')) {
      return [];
    }
    elseif ($order_item->field_json->isEmpty()) {
      return [];
    }
    return Json::decode($order_item->field_json->value);
  }

  /**
   * Init process.
   */
  private function initProcess() {
    $config = $this->configFactory->get('syncart.settings');
    $this->useCheckStoreAvailability = (bool) $config->get('check_store');
  }

  /**
   * Get data for product-feature-cart.
   */
  public function getData(
    ProductInterface $commerce_product,
    string $view_mode
  ) : array {
    $attribute_weight = $this->getAttributeWeight();

    $config = $this->configFactory->get('syncart.settings');
    $note = (bool) $config->get('note');
    $novariation = $config->get('no_variation_text') ?? '';
    $showquantity = $config->get('show_quantity') ?? 0;
    $currency_storage = \Drupal::entityTypeManager()->getStorage('commerce_currency');
    $currency_formatter = \Drupal::service('commerce_price.currency_formatter');
    $prices = [];
    $attributes = [];
    foreach ($commerce_product->variations as $item) {
      if (empty($item->entity)) {
        continue;
      }
      $variation = $item->entity;
      if (!is_object($variation->field_stock)) {
        $stock = 0;
      }
      else {
        $stock = (int) $variation->field_stock->value;
      }
      if (!is_object($variation) || (!$variation->status->value)) {
        continue;
      }
      $priceObject = $variation->getPrice();
      if (!is_object($priceObject)) {
        continue;
      }

      $attribute_item = [];
      foreach ($variation->getAttributeValues() as $key => $value) {
        $attribute_label = $value->getAttribute()->label();
        $attribute_original = $value->getAttribute()->getOriginalId();
        $attribute_item[] = [
          'id' => $value->id(),
          'label' => $attribute_label,
          'name' => $value->getName(),
        ];
        if ($this->useCheckStoreAvailability && $stock == 0) {
          continue;
        }
        $attribute = [
          'id' => $value->id(),
          'name' => $value->getName(),
          'label' => $attribute_original,
        ];
        if ($value->hasField('field_color') && !empty($value->get('field_color')->getValue()[0])) {
          $attribute['value'] = $value->get('field_color')->getValue()[0];
        }
        if ($value->hasField('field_hex') && !empty($value->get('field_hex')->getValue()[0])) {
          $attribute['value'] = $value->get('field_hex')->getValue()[0];
        }
        if (empty($attributes)) {
          $attributes[$attribute_weight[$attribute_original]][$attribute_label][] = $attribute;
        }
        if (empty($attributes[$attribute_weight[$attribute_original]][$attribute_label])) {
          $attributes[$attribute_weight[$attribute_original]][$attribute_label][] = $attribute;
        }
        elseif (!in_array($attribute, $attributes[$attribute_weight[$attribute_original]][$attribute_label])) {
          $attributes[$attribute_weight[$attribute_original]][$attribute_label][] = $attribute;
        }
      }
      $price = $priceObject->getNumber();
      $currency_code = $variation->getPrice()->getCurrencyCode();
      /** @var \Drupal\commerce_price\Entity\CurrencyInterface $currency */
      $currency = $currency_storage->load($currency_code);
      $old_price = FALSE;
      if ($variation->field_oldprice && $variation->field_oldprice->value) {
        $old_price = $variation->field_oldprice->value;
      }
      $catalog_name = '';
      if ($commerce_product->hasField('field_catalog') && $catalog = $commerce_product->field_catalog->entity) {
        $catalog_name = $catalog->getName();
      }
      $info['catalog'] = $catalog_name ?? '';
      $prices[] = [
        'vid' => $variation->id(),
        'product_id' => $commerce_product->id(),
        'status' => $variation->status->value,
        'title' => $variation->getTitle(),
        'price' => $price,
        'price_number' => $price,
        'old_price' => $old_price,
        'currency_code' => $currency_code,
        'price_format' => !empty($price) ? $currency_formatter->format($price, $currency_code, []) : '',
        'oldprice_format' => !empty($old_price) ? $currency_formatter->format($old_price, $currency_code, []) : '',
        'currency' => $currency->getSymbol(),
        'catalog' => $catalog_name ?? '',
        'attributes' => $attribute_item,
        'stock' => $stock,
        'stores' => $this->getStoreAvailability($variation),
      ];
    }
    ksort($attributes);
    $attributes_sorted = [];
    $first = TRUE;
    $master = [];
    foreach ($attributes as $sorted) {
      foreach ($sorted as $key => $value) {
        if ($first) {
          foreach ($value as $id) {
            $master[] = $id['id'];
          }
        }
        $attributes_sorted[$key] = $value;
      }
      $first = FALSE;
    }
    $prefix = $commerce_product->field_price_prefix->value;
    $config = \Drupal::config('synlanding.settings');
    if ($config) {
      $layout = $config->get('design');
    }
    return [
      'product' => $commerce_product,
      'settings' => [
        'status' => (int) $commerce_product->status->value,
        'unit' => $this->getUnitName($commerce_product),
        'qstep' => $this->getPackageStep($commerce_product),
        'fullstate' => $view_mode == 'full' ? TRUE : FALSE,
        'checkstore' => $this->useCheckStoreAvailability,
        'showquantity' => $showquantity,
        'novariation' => $novariation,
        'masterarray' => $master,
        'note' => $note,
        'features' => $this->getFeatures($commerce_product),
        'prefix' => $prefix,
        'layout' => $layout ?? 'default',
      ],
      'prices' => $prices,
      'attributes' => $attributes_sorted,
      'uid' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * Get product Features.
   */
  private function getFeatures(ProductInterface $commerce_product) : array {
    $syncart_product_feature_exists = \Drupal::service('module_handler')
      ->moduleExists('syncart_product_feature');
    if (!$syncart_product_feature_exists) {
      return [];
    }
    elseif (!$commerce_product->hasField('field_product_features')) {
      return [];
    }
    elseif ($commerce_product->field_product_features->isEmpty()) {
      return [];
    }
    $features = [];
    foreach ($commerce_product->field_product_features as $product_feature) {
      $features[] = $this->getFeatureData($product_feature->entity);
    }
    return array_values(
      array_filter($features)
    );
  }

  /**
   * Get Feature Data.
   */
  private function getFeatureData(ParagraphInterface $product_feature) :? array {
    $products = $this->getProductFeatures($product_feature);
    if (empty($products)) {
      return NULL;
    }
    return [
      'id' => $product_feature->id(),
      'title' => $product_feature->field_product_feature_title->value,
      'widget' => $product_feature->field_product_feature_widget->value,
      'none' => $product_feature->field_product_feature_none->value,
      'products' => $products,
    ];
  }

  /**
   * Check available in store.
   */
  private function checkAvailableInStore(ProductVariationInterface $variation) : bool {
    if (!$this->useCheckStoreAvailability) {
      return TRUE;
    }
    elseif (!$variation->hasField('field_stock')) {
      return TRUE;
    }
    return $variation->field_stock->value > 0;
  }

  /**
   * Get first Available variation.
   */
  public function getFirstAvailableVariation(
    ProductInterface $commerce_product
  ) :? ProductVariationInterface {
    if (!$this->useCheckStoreAvailability) {
      return $commerce_product->getDefaultVariation();
    }
    $variations = $commerce_product->getVariations();
    foreach ($variations as $variation) {
      if ($this->checkAvailableInStore($variation)) {
        return $variation;
      }
    }
    return NULL;
  }

  /**
   * Get product Features.
   */
  private function getProductFeatures(ParagraphInterface $product_feature) : array {
    $features = [];
    foreach ($product_feature->field_product_feature_products as $feature_product) {
      $commerce_product = $feature_product->entity;
      if (!$commerce_product->isPublished()) {
        continue;
      }
      $variation = $this->getFirstAvailableVariation($commerce_product);
      if (empty($variation)) {
        continue;
      }
      if ($variation->hasField('field_stock')) {
        $stock = $variation->field_stock->value;
      }
      $features[] = [
        'id' => (int) $commerce_product->id(),
        'title' => $commerce_product->getTitle(),
        'vid' => (int) $variation->id(),
        'price_number' => $variation->getPrice()
          ->getNumber(),
        'stock' => $stock ?? NULL,
      ];
    }
    return $features;
  }

  /**
   * Get Attribute Weight.
   */
  private function getStoreAvailability(ProductVariationInterface $variation) :? array {
    if (!$variation->hasField('field_stocks')) {
      return NULL;
    }
    elseif ($variation->field_stocks->isEmpty()) {
      return NULL;
    }
    $stores = [];
    foreach ($variation->field_stocks as $field_stocks) {
      if (empty($field_stocks->entity)) {
        continue;
      }
      $stock = $field_stocks->entity;
      if (!$stock->hasField('field_stores_store')) {
        continue;
      }
      elseif (empty($stock->field_stores_store->entity)) {
        continue;
      }
      $stock = $stock->field_stores_store->entity;
      if (!$stock->isPublished()) {
        continue;
      }
      $stores[] = [
        'name' => $stock->getName(),
        'quantity' => $stock->field_stores_stock->value,
      ];
    }
    return $stores;
  }

  /**
   * Get Attribute Weight.
   */
  private function getAttributeWeight() : array {
    $attribute_weight = [];
    $weight = 100;

    $config = $this->configFactory->get(
      'core.entity_view_display.commerce_product_variation.variation.default'
    );

    $variation_definitions = $config->getRawData()['content'];

    $attribute_original = $this->entityTypeManager
      ->getStorage('commerce_product_attribute')
      ->getQuery()
      ->accessCheck(TRUE)
      ->execute();
    foreach ($attribute_original as $key => $value) {
      if (array_key_exists("attribute_{$key}", $variation_definitions)) {
        $attribute_weight[$key] = $variation_definitions["attribute_{$key}"]['weight'];
      }
      else {
        if ($key == 'color') {
          $attribute_weight[$key] = 1;
        }
        else {
          $attribute_weight[$key] = $weight++;
        }
      }
    }
    return $attribute_weight;
  }

  /**
   * Get Package Step.
   */
  private function getPackageStep(ProductInterface $commerce_product) : float {
    if ($commerce_product->hasField('field_package')) {
      return $commerce_product->field_package->value ?? 1;
    }
    return 1;
  }

  /**
   * Get Unit Name.
   */
  private function getUnitName(ProductInterface $commerce_product) :? string {
    if (!$commerce_product->hasField('field_unit')) {
      return NULL;
    }
    elseif ($commerce_product->field_unit->isEmpty()) {
      return NULL;
    }
    elseif (empty($commerce_product->field_unit->entity)) {
      return NULL;
    }
    return $commerce_product->field_unit->entity->getName();
  }

}
