<?php

namespace Drupal\syncart\Service;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Component\Utility\Crypt;
use Drupal\Core\Site\Settings;
use Drupal\Core\Url;

/**
 * Checkout Link Manager service.
 */
class CheckoutLink {

  /**
   * Helper.
   */
  public function generateUrl(OrderInterface $order, $use_changed_time = TRUE) : Url {
    $timestamp = time();
    return new Url('syncart.checkout_link', [
      'commerce_order' => $order->id(),
      'timestamp' => $timestamp,
      'hash' => self::generateHash($timestamp, $order, $use_changed_time),
    ]);
  }

  /**
   * Helper.
   */
  public function generateHash($timestamp, OrderInterface $commerce_order, bool $use_changed_time = TRUE) : string {
    $changed_time_string = '';
    if ($use_changed_time) {
      $changed_time_string = $commerce_order->getChangedTime();
    }
    return Crypt::hmacBase64($timestamp . $commerce_order->id() . $changed_time_string, Settings::getHashSalt());
  }

}
