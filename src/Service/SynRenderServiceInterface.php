<?php

namespace Drupal\syncart\Service;

/**
 * Interface for SynRenderServiceInterface.
 */
interface SynRenderServiceInterface {

  /**
   * Подготовка данных для странциы корзины.
   *
   * @return array
   *   Структура корзины
   */
  public function data($cid = FALSE, array $render = []);

  /**
   * Проверка наличия товаров в корзине.
   *
   * @return bool
   *   Пуста ли корзина
   */
  public function isEmpty();

}
