<?php

namespace Drupal\syncart\Service;

use Drupal\commerce_cart\CartProviderInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_order\Entity\OrderItemInterface;
use Drupal\commerce_product\Entity\ProductVariationInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Custom Cart Service.
 */
class SynCartService implements SynCartServiceInterface {

  const ORDER_TYPE_DEFAULT = 'default';
  const ORDER_TYPE_POS = 'pos';
  const ORDER_ITEM_BUNDLE_DEFAULT = 'default';
  const ORDER_ITEM_BUNDLE_PRODUCT_FEATURE = 'product_feature';
  const STORE_ID = 1;
  const BCMATH_SCALE = 4;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Провайдер корзины.
   *
   * @var \Drupal\commerce_cart\CartProviderInterface
   */
  protected $cartProvider;

  /**
   * Объект текущей корзины.
   *
   * @var \Drupal\commerce_order\Entity\OrderInterface
   */
  protected $cart;

  /**
   * The product service.
   *
   * @var \Drupal\syncart\Service\ProductService
   */
  protected $productService;

  /**
   * Constructs a new SynCartService object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\commerce_cart\CartProviderInterface $cart_provider
   *   Провайдер корзины.
   * @param \Drupal\syncart\Service\ProductService $product_service
   *   The product service.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    ConfigFactoryInterface $config_factory,
    CartProviderInterface $cart_provider,
    ProductService $product_service
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->configFactory = $config_factory;
    $this->cartProvider = $cart_provider;
    $this->productService = $product_service;
    $this->initCart();
  }

  /**
   * Забрать резерва со склада.
   */
  public function orderReserveFromStock(OrderInterface $commerce_order) {
    if (!$this->configFactory->get('syncart.settings')->get('check_store')) {
      return;
    }
    foreach ($commerce_order->getItems() as $order_item) {
      $variation = $order_item->getPurchasedEntity();
      $quantity = $order_item->getQuantity();
      $this->orderReserveFromVariation($variation, $quantity);

      $selected_products = Json::decode($order_item->field_json->value ?? '[]');
      foreach ($selected_products as $selected_product) {
        /** @var \Drupal\commerce_product\Entity\ProductInterface $commerce_product */
        $commerce_product = $this->entityTypeManager
          ->getStorage('commerce_product')
          ->load($selected_product['id']);
        $variation = $this->productService->getFirstAvailableVariation(
          $commerce_product
        );
        $this->orderReserveFromVariation($variation, $quantity);
      }
    }
  }

  /**
   * Забрать резерва с вариации.
   */
  public function orderReserveFromVariation(
    ProductVariationInterface $variation,
    $quantity
  ) {
    $current_stock = (float) $variation->field_stock->value;
    $variation->set(
      'field_stock', bcsub($current_stock, $quantity, self::BCMATH_SCALE)
    );
    $variation->save();
  }

  /**
   * Получить текущую корзину пользователя.
   */
  public function load() {
    return $this->cart;
  }

  /**
   * Удалить товар из корзины.
   */
  public function removeOrderItem($order_item_id) {
    $order_item = OrderItem::load($order_item_id);
    if (is_object($order_item)) {
      $this->cart->removeItem($order_item);
      $order_item->delete();
      return $this->cart->save();
    }
    return FALSE;
  }

  /**
   * Получить доступный остаток по товару.
   */
  public function getOrderItemStock($vid) {
    $storage = \Drupal::entityTypeManager()->getStorage('commerce_product_variation');
    $variation = $storage->load($vid);
    return (float) $variation->field_stock->value;
  }

  /**
   * Обновить примечание позиции заказа.
   */
  public function setOrderItemNote($order_item_id, $note) {
    if (is_object($order_item = OrderItem::load($order_item_id))) {
      $order_item->setData('note', $note);
      $this->cart->save();
    }
  }

  /**
   * Обновить количество товара в корзине.
   */
  public function setOrderItemQuantity($order_item_id, $quantity = 0) {
    if (empty($order_item_id) || !is_numeric($order_item_id)) {
      return;
    }
    /** @var \Drupal\commerce_order\Entity\OrderItemInterface $commerce_order_item */
    $commerce_order_item = $this->entityTypeManager
      ->getStorage('commerce_order_item')
      ->load($order_item_id);
    if (!is_object($commerce_order_item)) {
      return;
    }
    if (\Drupal::config('syncart.settings')->get('check_store')) {
      $variation = $commerce_order_item->getPurchasedEntity();
      $current_stock = (float) $variation->field_stock->value;
      if ($quantity <= $current_stock) {
        $commerce_order_item->setQuantity($quantity);
      }
      else {
        $commerce_order_item->setQuantity($current_stock);
      }
    }
    else {
      $commerce_order_item->setQuantity($quantity);
    }
    $this->cart->save();
    $this->cart->recalculateTotalPrice();
    $this->cart->save();
  }

  /**
   * Перевод корзины в статус заказа.
   */
  public function cartToOrder() {
    $this->cart->set('state', 'completed');
    $this->cart->save();
    return $this->cart;
  }

  /**
   * {@inheritdoc}
   */
  private function getContactEmails() : array {
    $our_mail = \Drupal::config('contact_mail.settings')->get('emails');
    $x = str_replace(["\r\n", "\r", "\n"], " ", $our_mail);
    $x = str_replace(",", " ", $x);
    return array_diff(explode(' ', $x), ['', NULL, FALSE]);
  }

  /**
   * {@inheritdoc}
   */
  public function sendReceipt($order) {
    $profile = $order->getBillingProfile();
    if (!is_object($profile)) {
      return;
    }
    $mails = $this->getContactEmails();
    if (empty($mails)) {
      return;
    }
    $order_type_storage = \Drupal::entityTypeManager()->getStorage('commerce_order_type');
    /** @var \Drupal\commerce_order\Entity\OrderTypeInterface $order_type */
    $order_type = $order_type_storage->load($order->bundle());
    if ($order_type->get('sendReceipt') && $order->mail->value != $profile->field_customer_email->value) {
      $mails[] = $profile->field_customer_email->value;
    }

    $order_total_summary = \Drupal::service('commerce_order.order_total_summary');
    $body = [
      '#theme' => 'commerce_order_receipt',
      '#order_entity' => $order,
      '#totals' => $order_total_summary->buildTotals($order),
    ];

    $profile_view_builder = \Drupal::entityTypeManager()->getViewBuilder('profile');
    $body['#billing_information'] = $profile_view_builder->view($profile);
    $params = [
      'id' => 'order_receipt',
      'from' => $order->getStore()->getEmail(),
      'order' => $order,
    ];
    $customer = $order->getCustomer();
    if ($customer->isAuthenticated()) {
      $params['langcode'] = $customer->getPreferredLangcode();
    }

    $mail_handler = \Drupal::service('commerce.mail_handler');
    $subject = t('Order #@number confirmed', ['@number' => $order->getOrderNumber()]);
    foreach ($mails as $key => $value) {
      $mail_handler->sendMail($value, $subject, $body, $params);
    }
  }

  /**
   * Повторить заказ.
   */
  public function addOrderItem($commerce_order) {
    $cart_manager = \Drupal::service('commerce_cart.cart_manager');
    foreach ($commerce_order->getItems() as $order_item) {
      $variation = $order_item->getPurchasedEntity();
      $product = $variation ? $variation->getProduct() : NULL;
      if ($product && $product->isPublished()) {
        $order_item_new = $order_item->createDuplicate();
        $order_item_new->enforceIsNew();
        $order_item_new->id = NULL;
        $order_item_new->order_item_id = NULL;
        $order_item_new->save();
        $cart_manager->addOrderItem($this->cart, $order_item_new);
      }
    }
  }

  /**
   * User Authorization.
   */
  public function updateUserRegister($registration) {
    $config = \Drupal::configFactory()->getEditable('user.settings');
    if (is_object($config)) {
      if ($registration) {
        $config->set('register', 'visitors');
      }
      else {
        $config->set('register', 'admin_only');
      }
      $config->save(TRUE);
    }
  }

  /**
   * Add to cart.
   */
  public function setProductFeatureOrderItemQuantity(array $content_parameters) {
    if (empty($content_parameters['item_id'])) {
      return;
    }
    /** @var \Drupal\commerce_order\Entity\OrderItemInterface $commerce_order_item */
    $commerce_order_item = $this->entityTypeManager
      ->getStorage('commerce_order_item')
      ->load($content_parameters['item_id']);
    if (empty($commerce_order_item)) {
      return;
    }
    $this->addToCart([
      'vid' => $commerce_order_item->getPurchasedEntityId(),
      'quantity' => $content_parameters['quantity'] ?? NULL,
      'selected_products' => $content_parameters['selected_products'] ?? NULL,
    ]);
  }

  /**
   * Add to cart.
   */
  public function addToCart(array $content_parameters) {
    if (empty($content_parameters['vid'])) {
      return;
    }
    elseif (empty($content_parameters['quantity'])) {
      return;
    }
    $this->addItem(
      $this->getOrderItemBundle($content_parameters),
      $content_parameters['vid'],
      $content_parameters['quantity'],
      $content_parameters['selected_products'] ?? []
    );
  }

  /**
   * Получить тип позиции заказа.
   */
  private function getOrderItemBundle(array $content_parameters) : string {
    $syncart_product_feature_exists = \Drupal::service('module_handler')
      ->moduleExists('syncart_product_feature');
    if (!$syncart_product_feature_exists) {
      return self::ORDER_ITEM_BUNDLE_DEFAULT;
    }
    $selected_products = $content_parameters['selected_products'] ?? [];
    return empty($selected_products) ? self::ORDER_ITEM_BUNDLE_DEFAULT : self::ORDER_ITEM_BUNDLE_PRODUCT_FEATURE;
  }

  /**
   * Добавить товар в корзину.
   */
  private function addItem(
    string $bundle,
    int $vid,
    $quantity = 1,
    array $selected_products = []
  ) {
    $variation = $this->entityTypeManager
      ->getStorage('commerce_product_variation')
      ->load($vid);
    if (empty($variation)) {
      return;
    }
    $currency_code = FALSE;
    if ($this->cart->total_price->number > 0) {
      $currency_code = $this->cart->total_price->currency_code;
    }
    if ($currency_code && $currency_code != $variation->price->currency_code) {
      \Drupal::messenger()->addError(t("Wrong Currency"));
      return;
    }
    $this->setProductFeatureOrderItem($bundle, $variation, $quantity, $selected_products);
  }

  /**
   * Ограничить кол-во остатками на складе.
   */
  private function limitQuantityWithVariationStock(
    &$quantity,
    ProductVariationInterface $variation
  ) {
    $check_store = $this->configFactory->get('syncart.settings')
      ->get('check_store');
    if (!$check_store) {
      return;
    }
    $current_stock = (float) $variation->field_stock->value;
    if ($quantity > $current_stock) {
      $quantity = $current_stock;
    }
  }

  /**
   * Ограничить кол-во остатками на складе.
   */
  private function limitQuantityWithSelectedProductsStock(
    &$quantity,
    array $selected_products
  ) {
    $check_store = $this->configFactory->get('syncart.settings')
      ->get('check_store');
    if (!$check_store) {
      return;
    }
    foreach ($selected_products as $selected_product) {
      /** @var \Drupal\commerce_product\Entity\ProductInterface $commerce_product */
      $commerce_product = $this->entityTypeManager
        ->getStorage('commerce_product')
        ->load($selected_product['id']);
      $variation = $this->productService->getFirstAvailableVariation(
        $commerce_product
      );
      $this->limitQuantityWithVariationStock($quantity, $variation);
    }
  }

  /**
   * Если в заказе есть вариация, увеличить кол-во, иначе добавить новую.
   */
  private function setProductFeatureOrderItem(
    string $bundle,
    ProductVariationInterface $variation,
    $quantity,
    array $selected_products
  ) {
    $has_item = FALSE;
    $this->limitQuantityWithVariationStock($quantity, $variation);
    foreach ($this->cart->getItems() as $order_item) {
      if ($order_item->bundle() !== $bundle) {
        continue;
      }
      elseif ($order_item->data->getValue()) {
        continue;
      }
      elseif ($order_item->getPurchasedEntityId() !== $variation->id()) {
        continue;
      }
      $is_equal_selected_products = $this->orderItemHasEqualSelectedProducts(
        $order_item, $selected_products
      );
      if ($is_equal_selected_products) {
        if (!empty($selected_products)) {
          $this->limitQuantityWithSelectedProductsStock($quantity, $selected_products);
        }
        $order_item->setQuantity($quantity);
        $order_item->save();
        $has_item = TRUE;
        break;
      }
    }
    // Если вариация ранее не была добавлена, добавляем.
    if (!$has_item) {
      if (!empty($selected_products)) {
        $this->limitQuantityWithSelectedProductsStock($quantity, $selected_products);
      }
      /** @var \Drupal\commerce_order\Entity\OrderItemInterface $commerce_order_item */
      $commerce_order_item = $this->entityTypeManager
        ->getStorage('commerce_order_item')
        ->create([
          'type' => $bundle,
          'purchased_entity' => $variation,
          'quantity' => $quantity,
          'unit_price' => $variation->getPrice(),
        ]);
      if ($this->isProductFeatureOrderItem($commerce_order_item)) {
        $commerce_order_item->set('field_json', Json::encode($selected_products));
      }
      $commerce_order_item->save();
      $this->cart->addItem($commerce_order_item);
    }
    $this->cart->recalculateTotalPrice();
    $this->cart->save();
  }

  /**
   * Если в заказе есть вариация, увеличить кол-во, иначе добавить новую.
   */
  private function orderItemHasEqualSelectedProducts(
    OrderItemInterface $order_item,
    array $selected_products
  ) : bool {
    $field_exists = $order_item->hasField('field_json');
    $products_exists = !empty($selected_products);
    if ($field_exists && $products_exists) {
      $selected_products_json = $order_item->field_json->value ?? '[]';
      return Json::decode($selected_products_json) == $selected_products;
    }
    elseif (!$field_exists && !$products_exists) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Получение текущей корзины.
   */
  private function initCart() {
    $this->cart = $this->getCart();
    if (empty($this->cart)) {
      $this->createCart();
    }
    $this->clearCarts();
  }

  /**
   * Создать новую корзину.
   */
  private function getCart() {
    return $this->cartProvider->getCart($this->getOrderType());
  }

  /**
   * Создать новую корзину.
   */
  private function createCart() {
    $store = $this->entityTypeManager->getStorage('commerce_store')
      ->load(self::STORE_ID);
    $account = \Drupal::currentUser();
    $this->cart = $this->cartProvider->createCart(
      $this->getOrderType(), $store, $account
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getOrderType() {
    return $this->isPos() ? self::ORDER_TYPE_POS : self::ORDER_TYPE_DEFAULT;
  }

  /**
   * {@inheritdoc}
   */
  private function isPos() {
    $cache_context = \Drupal::request()->cookies->get('cache_context');
    if (
      \Drupal::service('module_handler')->moduleExists('syncart_pos') &&
      $cache_context === 'pos'
    ) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Эта позиция заказа "с комплектацией".
   */
  private function isProductFeatureOrderItem(
    OrderItemInterface $commerce_order_item
  ) : bool {
    return $commerce_order_item->bundle() == self::ORDER_ITEM_BUNDLE_PRODUCT_FEATURE;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $items = [];
    if (is_object($this->cart)) {
      $items = $this->cart->getItems();
    }
    return empty($items);
  }

  /**
   * Объединение и очистка дублирующихся корзин.
   */
  private function clearCarts() {
    $carts = $this->cartProvider->getCarts();
    $current_order_type = $this->getOrderType();
    if (count($carts) < 2) {
      return;
    }
    $goods = [];
    foreach ($carts as $other_cart) {
      if ($other_cart->id() == $this->cart->id()) {
        continue;
      }
      elseif ($other_cart->bundle() !== $current_order_type) {
        continue;
      }
      foreach ($other_cart->getItems() as $order_item) {
        $bundle = $order_item->bundle();
        $pid = $order_item->getPurchasedEntityId();
        $quantity = $order_item->quantity->value;
        if (isset($goods[$bundle][$pid])) {
          $goods[$bundle][$pid] += $quantity;
        }
        else {
          $goods[$bundle][$pid] = $quantity;
        }
        $other_cart->removeItem($order_item);
      }
      $other_cart->delete();
    }
    foreach ($goods as $bundle => $bundle_goods) {
      foreach ($bundle_goods as $pid => $quantity) {
        $this->addItem($bundle, $pid, $quantity);
      }
    }
  }

}
