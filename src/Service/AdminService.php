<?php

namespace Drupal\syncart\Service;

/**
 * Custom Cart Service.
 */
class AdminService {

  /**
   * The entity type manager.
   *
   * @var object
   */
  protected $numberPatternsStorage;

  /**
   * The entity type manager.
   *
   * @var object
   */
  protected $orderTypeStorage;

  /**
   * Construct.
   */
  public function __construct() {
    $this->orderTypeStorage = \Drupal::entityTypeManager()->getStorage('commerce_order_type');
    $this->numberPatternsStorage = \Drupal::entityTypeManager()->getStorage('commerce_number_pattern');
  }

  /**
   * Get Order Types.
   */
  public function getOrderTypes() {
    $entities = [];
    $ids = \Drupal::entityQuery('commerce_order_type')->accessCheck(TRUE)
      ->condition('status', 1)
      ->execute();
    if (!empty($ids)) {
      foreach ($this->orderTypeStorage->loadMultiple($ids) as $id => $entity) {
        $entities[$id] = $entity;
      }
    }
    return $entities;
  }

  /**
   * Get Number Patterns.
   */
  public function getNumberPatterns() {
    $entities = [];
    $ids = \Drupal::entityQuery('commerce_number_pattern')->accessCheck(TRUE)
      ->condition('status', 1)
      ->execute();
    if (!empty($ids)) {
      foreach ($this->numberPatternsStorage->loadMultiple($ids) as $id => $entity) {
        $entities[$id] = $entity;
      }
    }
    return $entities;
  }

  /**
   * Get Number Pattern Pattern.
   */
  public function getNumberPatternPattern(string $number_pattern_id) {
    return \Drupal::config("commerce_number_pattern.commerce_number_pattern.$number_pattern_id")->get('configuration.pattern');
  }

  /**
   * Get Number Pattern Initial Number.
   */
  public function getNumberPatternInitialNumber(string $number_pattern_id) {
    return \Drupal::config("commerce_number_pattern.commerce_number_pattern.$number_pattern_id")->get('configuration.initial_number');
  }

  /**
   * Get Number Pattern Sequence.
   */
  public function getNumberPatternSequence(string $number_pattern_id) {
    $query = \Drupal::database()->select('commerce_number_pattern_sequence', 'number_sequence');
    $query->addField('number_sequence', 'number');
    $query->condition('number_sequence.entity_id', $number_pattern_id);
    $res = $query->execute()->fetchAssoc();
    return $res['number'] ?? 0;
  }

  /**
   * Reset Number Pattern Sequence.
   */
  public function resetNumberPatternSequence(string $number_pattern_id) {
    $number_pattern = $this->numberPatternsStorage->load($number_pattern_id);
    /** @var \Drupal\commerce_number_pattern\Plugin\Commerce\NumberPattern\SequentialNumberPatternInterface $number_pattern_plugin */
    $number_pattern_plugin = $number_pattern->getPlugin();
    $number_pattern_plugin->resetSequence();
    $number_pattern->save();
  }

}
