<?php

namespace Drupal\syncart\Service;

use Drupal\Core\Config\ConfigManagerInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Component\Serialization\Yaml;

/**
 * Class Install Service.
 */
class InstallService {

  /**
   * The config manager.
   *
   * @var \Drupal\Core\Config\ConfigManagerInterface
   */
  protected $configManager;

  /**
   * The module extension list.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected $moduleExtensionList;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\ConfigManagerInterface $config_manager
   *   The config manager.
   * @param \Drupal\Core\Extension\ModuleExtensionList $module_extension_list
   *   The module extension list.
   */
  public function __construct(
    ConfigManagerInterface $config_manager,
    ModuleExtensionList $module_extension_list
  ) {
    $this->configManager = $config_manager;
    $this->moduleExtensionList = $module_extension_list;
  }

  /**
   * Update or install config.
   */
  public function updateOrInstallAdditionalConfig(string $config) {
    $module_path = $this->moduleExtensionList->getPath('syncart');
    $files = glob("$module_path/config/additional/$config*.yml");
    foreach ($files as $file) {
      $raw = file_get_contents($file);
      $value = Yaml::decode($raw);
      if (!is_array($value)) {
        throw new \RuntimeException(
          sprintf('Invalid YAML file %s'), $file
        );
      }

      $type = $this->configManager->getEntityTypeIdByName(basename($file));
      $entity_manager = $this->configManager->getEntityTypeManager();
      $definition = $entity_manager->getDefinition($type);
      $id_key = $definition->getKey('id');
      $id = $value[$id_key];

      /** @var \Drupal\Core\Config\Entity\ConfigEntityStorage $entity_storage */
      $entity_storage = $entity_manager->getStorage($type);
      $entity = $entity_storage->load($id);
      if ($entity) {
        $entity = $entity_storage->updateFromStorageRecord($entity, $value);
        $entity->save();
        $updated[] = $id;
      }
      else {
        $entity = $entity_storage->createFromStorageRecord($value);
        $entity->save();
        $created[] = $id;
      }
    }
    return [
      'udpated' => $updated ?? [],
      'created' => $created ?? [],
    ];
  }

}
