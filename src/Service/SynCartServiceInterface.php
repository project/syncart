<?php

namespace Drupal\syncart\Service;

/**
 * Interface for SynCartServiceInterface.
 */
interface SynCartServiceInterface {

  /**
   * Загрузка текущей корзины пользователя.
   *
   * @return object
   *   Объект корзины.
   */
  public function load();

  /**
   * Добавить в корзину.
   *
   * @var array $content_parameters
   *   Id вариации.
   */
  public function addToCart(array $content_parameters);

  /**
   * Добавить вариацию в корзину.
   *
   * @var $order_item_id
   *   Id вариации.
   */
  public function removeOrderItem($order_item_id);

  /**
   * Отправляем чек клиенту на почтку.
   *
   * @var \Drupal\commerce_order\Entity\Order $order
   *   Order.
   */
  public function sendReceipt($order);

  /**
   * Обновить количество товара в корзине.
   */
  public function setOrderItemQuantity($order_item_id, $quantity = 0);

  /**
   * Повторить заказ.
   */
  public function addOrderItem($commerce_order);

  /**
   * Проверка наличия товаров в корзине.
   *
   * @return bool
   *   Пуста ли корзина
   */
  public function isEmpty();

}
