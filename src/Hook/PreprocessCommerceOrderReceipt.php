<?php

namespace Drupal\syncart\Hook;

use Drupal\image\Entity\ImageStyle;

/**
 * Implements hook_preprocess_commerce_order_receipt.
 */
class PreprocessCommerceOrderReceipt {

  /**
   * Implements hook.
   */
  public static function hook(&$variables) {
    /** @var \Drupal\Core\Url $checkoutUrl */
    $checkoutUrl = \Drupal::service('syncart.checkout')->generateUrl($variables['order_entity']);
    $variables['checkout_url'] = $checkoutUrl
      ->setAbsolute()
      ->toString();
    $images = [];
    foreach ($variables['order_entity']->getItems() as $key => $item) {
      if (!empty($item->getPurchasedEntity()->getProduct()->field_image->entity)) {
        $uri = $item->getPurchasedEntity()->getProduct()->field_image->entity->getFileUri();
        $url = ImageStyle::load('product_cart')->buildUrl($uri);
      }
      $images[$item->id()] = $url ?? '';
    }
    $variables['item_image'] = $images;
  }

}
