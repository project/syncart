<?php

namespace Drupal\syncart\Hook;

/**
 * @file
 * Contains \Drupal\syncart\Hook\VariationPresave.
 */

use Drupal\commerce_price\Price;

/**
 * Hook Variation Presave.
 */
class VariationPresave {

  /**
   * Hook.
   */
  public static function hook($entity) {
    $product = $entity->getProduct();
    if (empty($product)) {
      return;
    }
    $variation_price = $entity->getPrice();
    $variation_number = $variation_price->getNumber();
    $variation_currency_code = $variation_price->getCurrencyCode();
    $stores = $product->getStores();
    if (!empty($stores)) {
      $currencies_codes = self::getCurrenciesCodes($stores);
      if (in_array($variation_currency_code, $currencies_codes)) {
        return;
      }
      $entity->setPrice(new Price($variation_number, array_shift($currencies_codes)));
    }
    else {
      $store = \Drupal::entityTypeManager()->getStorage('commerce_store')->loadDefault();
      $currency_code = $store->getDefaultCurrencyCode();
      if ($variation_currency_code == $currency_code) {
        return;
      }
      $entity->setPrice(new Price($variation_number, $currency_code));
    }
    return;
  }

  /**
   * Get Currencies Codes.
   */
  private static function getCurrenciesCodes(array $stores) {
    $currencies_codes = [];
    foreach ($stores as $store) {
      $currencies_codes[] = $store->getDefaultCurrencyCode();
    }
    return array_unique($currencies_codes);
  }

}
