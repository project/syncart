<?php

namespace Drupal\syncart\Hook;

/**
 * @file
 * Contains \Drupal\syncart\Hook\ProductPresave.
 */

/**
 * Hook product Presave.
 */
class ProductPresave {

  /**
   * Hook.
   */
  public static function hook($entity) {
    if (!$entity->hasField('field_stock')) {
      return;
    }
    if (self::checkType($entity) && \Drupal::config('syncart.settings')->get('check_store')) {
      $old_stock = $entity->field_stock->value ?? 0;
      $stock = 0;
      foreach ($entity->variations->getValue() as $v_data) {
        $variation = \Drupal::entityTypeManager()->getStorage('commerce_product_variation')->load($v_data['target_id']);
        $stock += $variation->field_stock->value ?? 0;
      }
      if ($old_stock != $stock) {
        $entity->set('field_stock', $stock);
      }
    }
    return;
  }

  /**
   * Check Entity Type Id.
   */
  private static function checkType($entity) {
    $result = FALSE;
    if (method_exists($entity, 'bundle')) {
      if ($entity->bundle() == 'product') {
        $result = TRUE;
      }
    }
    return $result;
  }

}
