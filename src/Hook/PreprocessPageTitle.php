<?php

namespace Drupal\syncart\Hook;

/**
 * PreprocessPage.
 */
class PreprocessPageTitle {

  /**
   * Implements hook_preprocess_page_title().
   */
  public static function hook(&$variables) {
    $attributes = \Drupal::request()->attributes;
    $route = $attributes->get('_route');
    $params = $attributes->get('_route_params');
    if ($route == 'commerce_checkout.form' && isset($params['step']) && $params['step'] == 'complete') {
      $variables['title'] = t('Thank you for your order!');
    }

    // POS.
    if ($route == 'commerce_checkout.form' && isset($params['step']) && $params['step'] == 'order_information') {
      $variables['title'] = t('Order payment');
    }
  }

}
