<?php

namespace Drupal\syncart\Hook;

/**
 * @file
 * Contains \Drupal\syncart\Hook\OrderPresave.
 */

/**
 * Hook order Presave.
 */
class OrderPresave {

  /**
   * Hook.
   */
  public static function hook($entity) {
    if (!self::checkType($entity)) {
      return;
    }
    $default_mail = "guest@localhost";
    if ($entity->mail->isEmpty() || $entity->mail->value == $default_mail) {
      $profile = $entity->billing_profile->entity;
      if (empty($profile)) {
        return;
      }
      $customer_email = $profile->field_customer_email->value ?? $default_mail;
      $entity->mail->setValue(
        trim($customer_email)
      );
    }
  }

  /**
   * Check Entity Type Id.
   */
  private static function checkType($entity) {
    $result = FALSE;
    if (method_exists($entity, 'bundle')) {
      if ($entity->bundle() == 'default') {
        $result = TRUE;
      }
    }
    return $result;
  }

}
