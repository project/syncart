<?php

namespace Drupal\syncart\Hook;

use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Commerce CheckoutFlow Alter.
 */
class FormCommerceCheckoutFlowMultistepDefaultAlter {

  /**
   * Hook preprocess.
   */
  public static function hook(&$form, $form_state) {
    $form['actions']['next']['#value'] = t('Next');
    $order = \Drupal::routeMatch()->getParameters()->get('commerce_order');
    $pos = FALSE;
    $order_type = $order->type->target_id;
    $form['#order_type'] = $order_type;
    if ($order_type == 'pos') {
      $pos = TRUE;
    }
    $bad_quantities = FALSE;
    // Step 1 Order Information.
    if (\Drupal::config('syncart.settings')->get('check_store')) {
      $bad_quantities = self::checkBadQuantities($order);
    }
    if ($form['#step_id'] == 'order_information') {
      $user_id = \Drupal::currentUser()->id();
      if ($user_id) {
        $order_storage = \Drupal::entityTypeManager()->getStorage('commerce_order');
        $ids = $order_storage
          ->getQuery()
          ->condition('uid', $user_id)
          ->sort('created', 'DESC')
          ->range(0, 1)
          ->accessCheck(TRUE)
          ->execute();
        if (!empty($ids)) {
          $id = array_shift($ids);
          $profile = $order_storage->load($id)->getBillingProfile();
          if (!empty($profile)) {
            if ($pos) {
              if ($name = $profile->field_customer_name->value) {
                $form['payment_information']['billing_information']['field_customer_name']['widget'][0]['value']['#default_value'] = $name;
              }
              else {
                $form['payment_information']['billing_information']['field_customer_name']['#access'] = FALSE;
              }
              if ($surname = $profile->field_customer_surname->value) {
                $form['payment_information']['billing_information']['field_customer_surname']['widget'][0]['value']['#default_value'] = $surname;
              }
              else {
                $form['payment_information']['billing_information']['field_customer_surname']['#access'] = FALSE;
              }
              if ($phone = $profile->field_customer_phone->value) {
                $form['payment_information']['billing_information']['field_customer_phone']['widget'][0]['value']['#default_value'] = $phone;
              }
              else {
                $form['payment_information']['billing_information']['field_customer_phone']['#access'] = FALSE;
              }
              if ($email = $profile->field_customer_email->value) {
                $form['payment_information']['billing_information']['field_customer_email']['widget'][0]['value']['#default_value'] = $email;
              }
              else {
                $form['payment_information']['billing_information']['field_customer_email']['#access'] = FALSE;
              }
            }
            else {
              $form['payment_information']['billing_information']['field_customer_name']['widget'][0]['value']['#default_value'] = $profile->field_customer_name->value;
              $form['payment_information']['billing_information']['field_customer_surname']['widget'][0]['value']['#default_value'] = $profile->field_customer_surname->value;
              $form['payment_information']['billing_information']['field_customer_phone']['widget'][0]['value']['#default_value'] = $profile->field_customer_phone->value;
              $form['payment_information']['billing_information']['field_customer_email']['widget'][0]['value']['#default_value'] = $profile->field_customer_email->value;
            }
          }
        }
      }
      if (!empty($form['shipping_information'])) {
        if (isset($form['shipping_information']['recalculate_shipping'])) {
          $form['shipping_information']['recalculate_shipping']['#attributes']['class'][] = 'js-hide';
        }
        $form['shipping_information']['#title'] = t('Contact information');
        $form['shipping_information']['shipments'][0]['shipping_method']['#prefix'] = '<div class="checkout-legend--div">' . t('Shipping Information') . '</div>';
        if (!empty($form['shipping_information']['shipping_profile']['select_address'])) {
          $form['shipping_information']['shipping_profile']['select_address']['#access'] = FALSE;
        }
        if (!empty($form['shipping_information']['shipping_profile']['copy_to_address_book'])) {
          unset($form['shipping_information']['shipping_profile']['copy_to_address_book']);
        }
      }
      elseif (!empty($form['payment_information'])) {
        $form['payment_information']['billing_information']['#prefix'] = '<div class="label">' . t('Contact information') . '</div>';
      }
      if (!empty($form['payment_information']['billing_information']['copy_to_address_book'])) {
        unset($form['payment_information']['billing_information']['copy_to_address_book']);
      }
      if (!empty($form['billing_information']['profile']['copy_to_address_book'])) {
        unset($form['billing_information']['profile']['copy_to_address_book']);
      }
      if (!empty($form['sidebar'])) {
        $form['sidebar']['#access'] = FALSE;
      }
      if ($bad_quantities) {
        $form['actions']['next']['#submit'][0] = 'Drupal\syncart\Hook\FormCommerceCheckoutFlowMultistepDefaultAlter::redirectToCart';
      }
      if (!empty($form['coupon_redemption'])) {
        $form['coupon_redemption']['#prefix'] = '<div class="checkout-legend--div">' . t('Bonus system') . '</div>';
      }
      if ($pos) {
        $form['payment_information']['billing_information']['#prefix'] = FALSE;
        $form['payment_information']['billing_information']['#type'] = 'hidden';
        $form['payment_information']['#title'] = FALSE;
        if (!$user_id || empty($profile)) {
          $form['payment_information']['billing_information']['#access'] = FALSE;
          $form['shipping_information']['shipping_profile']['#access'] = FALSE;
          $form['shipping_information']['#title'] = FALSE;
        }
      }
    }
    // Step 2 Review.
    if ($form['#step_id'] == 'review') {
      $form['actions']['next']['#prefix'] = $form['actions']['next']['#suffix'];
      unset($form['actions']['next']['#suffix']);
    }
    // Step 3 Payment.
    if ($form['#step_id'] == 'payment') {

    }
    // Step 4 Complete.
    if ($form['#step_id'] == 'complete') {
      $data = [];
      $order_items = $order->order_items;
      if (!empty($order_items)) {
        $config_synapse = \Drupal::config('synapse.settings');
        $data = [
          'transaction_id' => $order->order_id->value,
          'currency' => $order->total_price->currency_code,
          'value' => $order->total_price->number,
          'items' => [],
          'send_to' => $config_synapse->get('ga4-id') ?? '',
        ];
        foreach ($order_items as $id => $item) {
          $order_item = $item->entity;
          $product_info = \Drupal::service('syncart.render')->getVariationInfo($order_item);
          $product = [
            'item_id' => $product_info['vid'],
            'item_name' => $product_info['title'],
            'price' => $product_info['number'],
            'item_category' => $product_info['catalog'],
            'quantity' => $product_info['quantity'],
          ];
          $data['items'][] = $product;
        }
      }
      $form['#attached']['drupalSettings']['metrika']['dataLayerPurchase'] = $data;
      $form['#attached']['library'][] = 'syncart/ecommerce';
    }
  }

  /**
   *
   */
  public static function redirectToCart() {
    return (new RedirectResponse("/cart"))->send();
  }

  /**
   *
   */
  public static function redirectToLogin() {
    return (new RedirectResponse("/user/login"))->send();
  }

  /**
   *
   */
  public static function checkBadQuantities($order) {
    $order_item_storage = \Drupal::entityTypeManager()->getStorage('commerce_order_item');
    foreach ($order->order_items->getValue() as $item_data) {
      $item = $order_item_storage->load($item_data['target_id']);
      $variation = $item->purchased_entity->entity;
      if ((float) $item->quantity->value > (float) $variation->field_stock->value) {
        return TRUE;
      }
    }
    return FALSE;
  }

}
