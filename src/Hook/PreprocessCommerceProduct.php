<?php

namespace Drupal\syncart\Hook;

/**
 * Hook preprocess page class.
 */
class PreprocessCommerceProduct {

  /**
   * Hook.
   */
  public static function hook(&$variables) {
    $config = \Drupal::config('synlanding.settings');
    $layout = $config->get('design');
    $variables['cart'] = [
      '#theme' => 'syncart-cart-field',
      '#data' => \Drupal::service('syncart.product')
        ->getData(
          $variables['product_entity'],
          $variables['elements']['#view_mode']
      ),
    ];
    $variables['favorite'] = [
      '#theme' => 'syncart-favorite-field',
      '#data' => [
        'uid' => \Drupal::currentUser()->id(),
        'product_id' => $variables['product_entity']->id(),
        'layout' => $layout,
        'view_mode' => $variables['elements']['#view_mode'],
        'favorites' => \Drupal::routeMatch()->getRouteName() == 'syncart.favorites',
      ],
    ];
  }

}
