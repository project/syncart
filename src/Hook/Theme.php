<?php

namespace Drupal\syncart\Hook;

/**
 * Hooks.
 */
class Theme {

  /**
   * Implements hook_theme().
   */
  public static function hook() {
    return [
      'syncart-cart-field' => [
        'template' => 'cart--field',
        'variables' => ['data' => []],
      ],
      'syncart-cart' => [
        'template' => 'cart',
        'variables' => ['data' => []],
      ],
      'syncart-cart-small' => [
        'template' => 'cart--small',
        'variables' => ['data' => []],
      ],
      'syncart-favorite-field' => [
        'template' => 'favorite--field',
        'variables' => ['data' => []],
      ],
      'syncart-favorites' => [
        'template' => 'favorites',
        'variables' => ['data' => []],
      ],
      'commerce_checkout_completion_message' => [
        'template' => 'commerce-checkout-completion-message',
        'base hook' => 'commerce_checkout_completion_message',
      ],
      'commerce_checkout_form' => [
        'template' => 'commerce-checkout-form',
        'base hook' => 'commerce_checkout_form',
      ],
      'syncart_bonus' => [
        'template' => 'syncart-bonus',
        'variables' => ['data' => []],
      ],
      'commerce_order_receipt' => [
        'template' => 'commerce-order-receipt',
        'base hook' => 'commerce_order_receipt',
      ],
      'commerce_order__user' => [
        'template' => 'commerce-order--user',
        'base hook' => 'commerce_order__user',
      ],
      'commerce_order__admin' => [
        'template' => 'commerce-order--admin',
        'base hook' => 'commerce_order__admin',
      ],
    ];
  }

}
