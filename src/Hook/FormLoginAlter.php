<?php

namespace Drupal\syncart\Hook;

/**
 * Hook form alter.
 */
class FormLoginAlter {

  /**
   * Implements hook_form_alter().
   */
  public static function hook(&$form, &$form_state) {
    if ($form['#form_id'] == 'user_login_form') {
      $form['#prefix'] = '<h1 class="user-login-title">Вход в личный кабинет</h1>';
      $form['pass']['#suffix'] = '<a class="user-login-forgot-link" href="/user/password">Забыли пароль?</a>';
    }
  }

  /**
   * Redirect to checkout.
   */
  public static function redirectToLogin() {
    return (new RedirectResponse("/user/login"))->send();
  }

}
