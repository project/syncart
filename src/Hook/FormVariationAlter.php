<?php

namespace Drupal\syncart\Hook;

/**
 * Hook form alter.
 */
class FormVariationAlter {

  /**
   * Implements hook_form_alter().
   */
  public static function hook(&$form, &$form_state, $form_id) {
    if (in_array($form_id, ['commerce_product_variation_variation_add_form', 'commerce_product_variation_variation_edit_form'])) {
    $storage = \Drupal::entityTypeManager()->getStorage('commerce_product_attribute_value');
    foreach ($form as $key => $formValue) {
      if (strpos($key, 'attribute_') !== FALSE) {
        $data = [];
        $ids = $storage->getQuery()
          ->condition('attribute', substr($key, 10))
          ->accessCheck(TRUE)
          ->execute();
        if (!empty($ids)) {
          foreach ($storage->loadMultiple($ids) as $id => $entity) {
            $data[] = [
              'id' => $entity->id(),
              'value' => $entity->name->value,
              'weight' => $entity->weight->value,
            ];
          }
        }
        $weight = array_column($data, 'weight');
        array_multisort($weight, $data);
        $options = ['_none' => '- Выберите значение -'];
        foreach ($data as $value) {
          $options[$value['id']] = $value['value'];
        }
        $form[$key]['widget']['#options'] = $options;
      }
    }
  }
  }

}
