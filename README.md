# _Syncart_

## Чем занимается?

> Заменяет виджет и дополняет функционал корзины из модуля Commerce
> Добавляет настройки для фугкционала корзины

## Требования к платформе

- _Drupal 8-11_
- _PHP 7.4.0+_

## Подмодули расширяющие функционал

- _order_status_ - Adds order statuses to syncart.
- _pos_ - Pos functionallity to syncart.
- _product_feature_ - Provides additional functionality for the site.

## Нужны модули

- commerce:commerce_cart
- commerce:commerce_checkout
- commerce:commerce_order

## Прочие зависимости

- модуль использует Vue.js
- модуль использует Vue-cookies

## Версии

- [Drupal.org prod версия](https://www.drupal.org/project/syncart)

```sh
composer require 'drupal/syncart'
```

- [gitlab dev версия](https://git.synapse-studio.ru/d-org/syncart)

```sh
https://git.synapse-studio.ru/d-org/syncart.git
```

## Как использовать?

Модуль добавляет фронтенд компоненты:

- Компонент для commerce cart для карточек и страниц товара _cart-field_
- Компонент для процесса оформления заказа _cart_
- Компонент для вывода общей информации о корзине _cart-small_
- Компонент с избранным _favorites_

Также модуль отправляет отренддеренный в соответствии с темой чек клиенту.

### Первоначальная настройка

- Форма настроек модуля _/admin/config/syncart_
  #### Форма
  - [x] _Добавлять учет складских остатков_
  - [x] Текст для товаров без вариаций
  - [x] Сбор пожертвований
  - [x] Ригистрация пользователей
  - [x] Показывать виджет-счетчик для добавляемых в корзину товаров

# Пока не создавайте тег для drupal.org

Altering:

```php
<?
/**
 * Implements hook_syncart_variation_alter().
 */
function HOOK_syncart_variation_alter(&$info, $variation) {
  \Drupal::messenger()->addStatus(__FUNCTION__);
  // Variation.
  foreach (['field_variation_color', 'field_variation_size'] as $field) {
    $term = $variation->$field->entity;
    $info[$field] = is_object($term) ? $term->getName() : '';
  }
  // Product.
  if (is_object($product = $variation->getProduct())) {
    $info['code'] = $product->field_code->value;
  }
}
```
